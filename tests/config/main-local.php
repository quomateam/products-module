<?php

$host = 'localhost';
$port = 3306;
$username = 'root';
$password = '4984313';

return [
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => "mysql:host=$host;dbname=base_web",
            'username' => $username,
            'password' => $password,
            'charset' => 'utf8',
        ],
        'db_config' => [
            'class' => 'yii\db\Connection',
            'dsn' => "mysql:host=$host;dbname=base_web_config",
            'username' => $username,
            'password' => $password,
            'charset' => 'utf8',
        ],
        'db_general' => [
            'class' => 'yii\db\Connection',
            'dsn' => "mysql:host=$host;dbname=base_web_general",
            'username' => $username,
            'password' => $password,
            'charset' => 'utf8',
        ],
        'db_media' => [
            'class' => 'yii\db\Connection',
            'dsn' => "mysql:host=$host;dbname=base_web_media",
            'username' => $username,
            'password' => $password,
            'charset' => 'utf8',
        ],
        'db_product' => [
            'class' => 'yii\db\Connection',
            'dsn' => "mysql:host=$host;dbname=base_web_product",
            'username' => $username,
            'password' => $password,
            'charset' => 'utf8',
        ],
        'db_company' => [
            'class' => 'yii\db\Connection',
            'dsn' => "mysql:host=$host;dbname=base_web_company",
            'username' => $username,
            'password' => $password,
            'charset' => 'utf8',
        ],
        'db_checkout' => [
            'class' => 'yii\db\Connection',
            'dsn' => "mysql:host=$host;dbname=base_web_checkout",
            'username' => $username,
            'password' => $password,
            'charset' => 'utf8',
        ],
        'db_checkout_payment' => [
            'class' => 'yii\db\Connection',
            'dsn' => "mysql:host=$host;dbname=base_web_checkout_payment",
            'username' => $username,
            'password' => $password,
            'charset' => 'utf8',
        ],
         'db_checkout_site_access' => [
            'class' => 'yii\db\Connection',
            'dsn' => "mysql:host=$host;dbname=base_web_checkout_site_access",
            'username' => $username,
            'password' => $password,
            'charset' => 'utf8',
        ],
        'db_tracking' => [
            'class' => 'yii\db\Connection',
            'dsn' => "mysql:host=$host;dbname=base_web_tracking",
            'username' => $username,
            'password' => $password,
            'charset' => 'utf8',
        ],
        'frontendUrlManager' => [
            'class' => 'yii\web\urlManager',
            'baseUrl' => 'http://localhost:90',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',

            // send all mails to a file by default. You have to set'secret'
            // 'useFileTransport' to false and configure a transport'secret'
            // for the mailer to send real emails.'secret'
            'useFileTransport' => false
        ],
    ],
    'modules' => [
        'media' => [
            'components' => [
                'db' => [
                    'class' => 'yii\db\Connection',
                    'dsn' => "mysql:host=$host;dbname=base_web_media",
                    'username' => $username,
                    'password' => $password,
                    'charset' => 'utf8',
                ],
            ],
        ]
    ]
];
