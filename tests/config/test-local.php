<?php

$test_host = 'localhost';

return yii\helpers\ArrayHelper::merge(
    require(__DIR__ . '/main.php'),
    require(__DIR__ . '/main-local.php'),
    require(__DIR__ . '/test.php'),
    [
        'components' => [
            'db' => [
                'dsn' => "mysql:host=$test_host;dbname=base-web_test",
            ],
            'db_config' => [
                'dsn' => "mysql:host=$test_host;dbname=base-web_config_test",
            ],
            'db_general' => [
                'dsn' => "mysql:host=$test_host;dbname=base-web_general_test",
            ],
            'db_media' => [
                'dsn' => "mysql:host=$test_host;dbname=base-web_media_test",
            ],
            'db_product' => [
                'dsn' => "mysql:host=$test_host;dbname=base-web_product_test",
            ],
            'db_company' => [
                'dsn' => "mysql:host=$test_host;dbname=base-web_company_test",
            ],
            'db_checkout' => [
                'dsn' => "mysql:host=$test_host;dbname=base-web_checkout_test",
            ],
            'db_checkout_payment' => [
                'dsn' => "mysql:host=$test_host;dbname=base-web_checkout_payment_test",
            ],
            'db_checkout_site_access' => [
                'dsn' => "mysql:host=$test_host;dbname=base-web_checkout_site_access_test",
            ],
        ],
    ]
);
