<?php

return [
    'vendorPath' => '../../../../vendor',
    'components' => [
        'formatter' => [
            'dateFormat' => 'dd/MM/yyyy',
        ],
        'frontendUrlManager' => [
            'class' => \yii\web\UrlManager::className(),
            'baseUrl' => '//'
        ],

    ],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'bootstrap' => [

    ],
    'modules' => [
        'config' => [
            'class' => quoma\modules\config\ConfigModule::className()
        ],
        'product' => [
            'class' => quoma\products\ProductsModule::className(),
        ],

    ],
];
