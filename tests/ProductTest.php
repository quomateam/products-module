<?php

namespace ProductTests;

use quoma\products\models\Product;
use quoma\products\models\Unit;
use quoma\products\ProductsModule;

class ProductTest extends \Codeception\Test\Unit
{
    /**
     * @var \ProductTests\UnitTester
     */
    protected $tester;
    
    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests
    public function testFailSaveEmptyProduct(){
        $product= new Product();

        $this->tester->assertFalse($product->save());
    }

    public function testSuccessSaveProduct(){
        $unit= new Unit([
            'name' => 'Test',
            'type' => 'int',
            'symbol_position' => 'suffix'
        ]);

        $unit->save();

        $product= new Product([
            'name' => 'Test',
            'unit_id' => $unit->unit_id,
            'status' => 'enabled',
            'width' => 1,
            'height' => 1,
            'large' => 1,
            'weight' => 1,
            'website_id' => 1
        ]);

        $this->tester->assertTrue($product->save());
    }

    public function testSuccessSaveNotRequiredDimensions(){

        $module= \Yii::$app->getModule('product')->required_dimensions= false;

        $unit= new Unit([
            'name' => 'Test',
            'type' => 'int',
            'symbol_position' => 'suffix'
        ]);

        $unit->save();


        $product= new Product([
            'name' => 'Test',
            'unit_id' => $unit->unit_id,
            'status' => 'enabled',
            'website_id' => 1
        ]);

        $this->tester->assertTrue($product->save());
    }

    public function testFailSaveRequiredDimensions(){

        //$module= \Yii::$app->getModule('product')->required_dimensions= false;

        $unit= new Unit([
            'name' => 'Test',
            'type' => 'int',
            'symbol_position' => 'suffix'
        ]);

        $unit->save();


        $product= new Product([
            'name' => 'Test',
            'unit_id' => $unit->unit_id,
            'status' => 'enabled',
            'website_id' => 1
        ]);

        $this->tester->assertFalse($product->save());
    }

    public function testSuccessSaveRequiredDuration(){
        \Yii::$app->getModule('product')->required_dimensions= false;
        \Yii::$app->getModule('product')->required_duration= true;

        $unit= new Unit([
            'name' => 'Test',
            'type' => 'int',
            'symbol_position' => 'suffix'
        ]);

        $unit->save();


        $product= new Product([
            'name' => 'Test',
            'unit_id' => $unit->unit_id,
            'status' => 'enabled',
            'duration' => 60,
            'website_id' => 1
        ]);

        $this->tester->assertTrue($product->save(), print_r($product->getErrors(), 1));
    }

    public function testFailSaveRequiredDuration(){
        \Yii::$app->getModule('product')->required_duration= true;

        $unit= new Unit([
            'name' => 'Test',
            'type' => 'int',
            'symbol_position' => 'suffix'
        ]);

        $unit->save();


        $product= new Product([
            'name' => 'Test',
            'unit_id' => $unit->unit_id,
            'status' => 'enabled',
            'website_id' => 1
        ]);

        $this->tester->assertFalse($product->save());
    }
}