<?php

namespace quoma\products\components;
use Yii;
use execut\widget\TreeFilterInput;

/**
 * Description of ArticleCategoryTree
 *
 * @author martin
 */
class ProductCategoryTree extends \execut\widget\TreeView{
    
    public $categories;


    /**
     * Widget header
     *
     * @var string
     */
    public $header = null;
    
    public function init() {
        
        $this->data = $this->tree($this->categories);

        if($this->header === null){
            $this->header = Yii::t('app', 'Categories');
        }elseif($this->header === false){
            $this->header = '';
        }
        
        parent::init();
        
    }
    
    protected function renderSearchWidget() {
        $options = $this->searchOptions;
        $options['treeViewId'] = $this->id;
        if (empty($options['inputOptions'])) {
            $options['inputOptions'] = [];
        }

        if (empty($options['inputOptions']['placeholder'])) {
            $options['inputOptions']['placeholder'] = Yii::t('app','Search...');
        }

        return TreeFilterInput::widget($options);
    }
   
    public function tree($categories){
        
        $items = [];
        
        foreach($categories as $category){
            
            $children = $category->categories;
            $parent = $category->parent;

            if($children){
                
                $items[] = [
                    'text' => $category->name,
                    'href' => $category->getUrl(),
                    'nodes' => $this->tree($children)
                ];
                
            }else{
                
                $items[] = [
                    'text' => $category->name,
                    'href' => $category->getUrl()
                ];
                
            }
        }
        return $items;
    }
    
    protected function _getUnnamespacedClassName($className = null)
    {
        if ($className === null) {
            $className = \execut\widget\TreeView::className();
        }

        $parts = explode('\\', $className);
        $name = $parts[count($parts) - 1];
        return $name;
    }

    protected function _registerBundle()
    {
        $bundleClass = \execut\widget\TreeView::className() . 'Asset';
        if (class_exists($bundleClass)) {
            $bundleClass::register($this->view);
        }
    }

    /**
     * @return string
     */
    protected function getCssClass()
    {
        $parts = explode('\\', \execut\widget\TreeView::className());
        $cssClass = Inflector::camel2id($parts[count($parts) - 1]);
        return $cssClass;
    }
    
}
