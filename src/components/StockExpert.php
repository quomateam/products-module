<?php

namespace  quoma\products\components;

use Yii;
use quoma\products\models\StockMovement;
use \frontend\models\Cart;

/**
 * Description of StockExpert
 *
 * @author mmoyano
 */
class StockExpert extends \yii\base\Component{
    
    public $optimize = false;
    
    public function createMove()
    {
        $sm = Yii::createObject([
            'class' => StockMovement::className(),
        ]);
        
        $sm->on(StockMovement::EVENT_BEFORE_INSERT, function($event){
            Yii::$app->getModule('product')->stock->setNewStock($event->sender);
        });
        
        return $sm;
    }
    
    public function createMoveIn()
    {
        $sm = $this->createMove();
        $sm->type = 'in';
        return $sm;
    }
    
    private function createMoveOut()
    {
        $sm = $this->createMove();
        $sm->type = 'out';
        return $sm;
    }
    
    //Pedido por ingresar
    private function createReservationIn()
    {
        $sm = $this->createMove();
        $sm->type = 'r_in';
        return $sm;
    }
    
    private function createReservationOut()
    {
        $sm = $this->createMove();
        $sm->type = 'r_out';
        return $sm;
    }
    
    /**
     * Efectua el registro de movimientos de stock vinculados a un carrito
     * de acuerdo al tipo del comprobante, derivando el control a una funcion
     * especifica por tipo de comprobante.
     * Transaccional.
     * @param frontend\models\Cart $cart
     * @throws \Exception
     * @throws \yii\web\HttpException
     */
    public function register($cart)
    {
        
        $method = 'registerReservation';
        
        if($cart->status  == Cart::STATUS_PAID){
           $method = 'registerSale';
        }
        
        if(method_exists($this, $method)){
        
            $connection = Yii::$app->db;
            $transaction = $connection->beginTransaction();
            
            try{
                
                $count = $this->$method($cart);
                $transaction->commit();
                 
                return $count;
                
            } catch (\Exception $e) {
                 
                $transaction->rollBack();
                var_dump($e);die;
                throw $e;
            }
        
        }
            
        throw new \yii\web\HttpException(500, Yii::t('Stock expert could not manage this class.'));
        
    }
    
    /**
     * Registra una reseva de stock
     * @param frontend/models/Cart $cart
     * @return int
     */
    private function registerReservation($cart)
    {
        $count = 0;
        Yii::trace($cart->cart_id,'LOG-ON-registerReservation');
        
        foreach($cart->cartHasProducts as $cart_product){
            
            $product = $cart_product->product;
            
            $stockMovement = $this->createReservationOut();
            $stockMovement->qty = $cart_product->qty;
            $stockMovement->cart_id = $cart->cart_id;
            $stockMovement->website_id = $product->website_id;
//            $stockMovement->website = $product->website;
            $stockMovement->product_id = $product->product_id;                
            $stockMovement->concept = $product->name;
            $stockMovement->active = 1;
           
            if($stockMovement->save()){
                $count++;
            }
        }
        
        return $count;
        
    }
    
    /**
     * Registra una venta
     * @param frontend/models/Cart $cart
     * @return int
     */
    private function registerSale($cart)
    {
        $count = 0;
         
        //Registro la salida
        foreach($cart->cartHasProducts as $cart_product){
            
            $product = $cart_product->product;
            $stockMovement = $this->createMoveOut();
            $stockMovement->qty = $cart_product->qty;
            $stockMovement->cart_id = $cart->cart_id;
            $stockMovement->product_id = $product->product_id;   
            $stockMovement->website_id = $product->website_id;
            $stockMovement->concept = $product->name;
            $stockMovement->active = 1;
            if($stockMovement->save()){
                $count++;
            }
        }
          
         //Desactivo las reservas de stock para este carrito
        \Yii::$app->db_product->createCommand()->update('stock_movement', ['active' => 0], ['cart_id' => $cart->cart_id,'type' => 'r_out'])->execute();
        
        return $count;
        
    }
    
    /**
     * Devuelve el stock del producto para la empresa dada, en base al registro
     * disponible en el ultimo movimiento de stock. Esto es asi para evitar
     * calculos en exceso. Si no se especifica empresa, devuelve el balance
     * registrado en el producto, que representa el balance total).
     * @param quoma\products\models\Product $product
     * @param \frontend\models\Website $website
     * @param boolean $secondary
     * @return double
     */
    public function getStock($product, $website = null, $secondary = false)
    {
        
        //Attr
        $qty_attr = 'qty';
        if($secondary){
            $qty_attr = 'secondary_qty';
        }
        
        if($website == null){
            if($secondary){
                return $product->secondary_balance;
            }else{
                return $product->balance;
            }
        }
        
        //Last move
        $mv = StockMovement::find()->where([
            'active'=>1, 
            'product_id' => $product->product_id,
            'type'=>['in','out']
        ])->andWhere(['website_id'=>$website->website_id])->cache(1)->all();

        $stock = 0;

        foreach ($mv as $m) {
            if ($m->type === 'in') {
                $stock = $stock + $m->$qty_attr;
            }else {
                $stock = $stock - $m->$qty_attr;
            }
        }

        Yii::info('Stock: '. $stock);
        
        return $stock;
        
    }
    
    /**
     * Devuelve el stock disponible del producto para la empresa dada. Toma el
     * stock real en base al registro disponible en el ultimo movimiento de 
     * stock, a traves de getStock(). A esto, agrega valores reservados, a traves
     * de getReservedStock().
     * @param quoma\products\models\Product $product
     * @param \frontend\models\Website $website
     * @param boolean $secondary
     * @return double
     */
    public function getAvaibleStock($product, $website = null, $secondary = false)
    {
        
        return $this->getStock($product, $website, $secondary) 
                - $this->getReservedStock($product, $website, $secondary);
        
    }
    
    /**
     * Devuelve el stock actualmente reservado. El calculo es exhaustivo.
     * Se debe calcular (al menos diariamente (TODO)), porque varía de acuerdo
     * a los vencimientos de las reservas efectuadas.
     * @param quoma\products\models\Product $product
     * @param \frontend\models\Website $website
     * @param boolean $secondary
     * @return double
     */
    public function getReservedStock($product, $website = null, $secondary = false)
    {
        
        //Attr
        $qty_attr = 'qty';
        if($secondary){
            $qty_attr = 'secondary_qty';
        }
        
       //In
        $in = StockMovement::find()->where([
            'active'=>1,
            'product_id' => $product->product_id,
            'type'=>'r_in'
        ])->andFilterWhere(['website_id'=>$website ? $website->website_id : null])
            ->andWhere('expiration_timestamp>'.time().' OR expiration_timestamp IS NULL')->sum($qty_attr);
        
        //Out
        $out = StockMovement::find()->where([
            'active'=>1, 
            'product_id' => $product->product_id,
            'type'=>'r_out'
        ])->andFilterWhere(['website_id'=>$website ? $website->website_id : null])
            ->andWhere('expiration_timestamp>'.time().' OR expiration_timestamp IS NULL')->sum($qty_attr);

        Yii::info('Out: ' . $out );
        Yii::info('In: ' . $in );

        return $out - $in ;
        
    }
    
    /**
     * Completa el registro de stock total del movimiento $stockMovement para
     * optimizar el acceso a datos.
     * @param type $stockMovement
     * @throws \yii\web\HttpException
     */
    public function setNewStock($stockMovement){
        
        if(!$stockMovement->isNewRecord){
            throw new \yii\web\HttpException(500, 'Stock movement must be new.');
        }
        
        if($stockMovement->type == 'in'){
            $stockMovement->stock = $this->getStock($stockMovement->product, $stockMovement->website) + $stockMovement->qty;
            $stockMovement->secondary_stock = $this->getStock($stockMovement->product, $stockMovement->website, true) + $stockMovement->secondary_qty;
        
            $stockMovement->avaible_stock = $this->getAvaibleStock($stockMovement->product, $stockMovement->website);
            $stockMovement->secondary_avaible_stock = $this->getAvaibleStock($stockMovement->product, $stockMovement->website, true);
        }
        
        if($stockMovement->type == 'out'){
            $stockMovement->stock = $this->getStock($stockMovement->product, $stockMovement->website) - $stockMovement->qty;
            $stockMovement->secondary_stock = $this->getStock($stockMovement->product, $stockMovement->website, true) - $stockMovement->secondary_qty;
        
            $stockMovement->avaible_stock = $this->getAvaibleStock($stockMovement->product, $stockMovement->website);
            $stockMovement->secondary_avaible_stock = $this->getAvaibleStock($stockMovement->product, $stockMovement->website, true);
        }
        
        if($stockMovement->type == 'r_in'){
            $stockMovement->stock = $this->getStock($stockMovement->product, $stockMovement->website);
            $stockMovement->secondary_stock = $this->getStock($stockMovement->product, $stockMovement->website, true);
        
            $stockMovement->avaible_stock = $this->getAvaibleStock($stockMovement->product, $stockMovement->website) + $stockMovement->qty;
            $stockMovement->secondary_avaible_stock = $this->getAvaibleStock($stockMovement->product, $stockMovement->website, true) + $stockMovement->secondary_qty;
        }
        
        if($stockMovement->type == 'r_out'){
            $stockMovement->stock = $this->getStock($stockMovement->product, $stockMovement->website);
            $stockMovement->secondary_stock = $this->getStock($stockMovement->product, $stockMovement->website, true);
        
            $stockMovement->avaible_stock = $this->getAvaibleStock($stockMovement->product, $stockMovement->website) - $stockMovement->qty;
            $stockMovement->secondary_avaible_stock = $this->getAvaibleStock($stockMovement->product, $stockMovement->website, true) - $stockMovement->secondary_qty;
        }
                
    }
    
    /**
     * Calcula el stock recorriendo todos los movimientos de stock disponibles.
     * @param type $product
     * @param type $website
     * @param type $secondary
     * @return type
     */
    public function calculateStock($product, $website = null, $secondary = false)
    {
        
        //Attr
        $qty_attr = 'qty';
        if($secondary){
            $qty_attr = 'secondary_qty';
        }
        
        //In
        $in = StockMovement::find()->where([
            'active'=>1, 
            'product_id' => $product->product_id,
            'type'=>'in'
        ])->andFilterWhere(['website_id'=>$website ? $website->website_id : null])->sum($qty_attr);
        
        //Out
        $out = StockMovement::find()->where([
            'active'=>1, 
            'product_id' => $product->product_id,
            'type'=>'out'
        ])->andFilterWhere(['website_id'=>$website ? $website->website_id : null])->sum($qty_attr);
        
        return $in - $out;
        
    }
    
    /**
     * Calcula el stock recorriendo todos los movimientos de stock disponibles.
     * @param quoma\products\models\Product $product
     * @param \frontend\models\Website $website
     * @param boolean $secondary
     * @return double
     */
    public function calculateAvaibleStock($product, $website = null, $secondary = false)
    {
        
        return $this->calculateStock($product, $website, $secondary) 
                - $this->getReservedStock($product, $website, $secondary);
        
    }
    
    /**
     * Este metodo existe para complementar los metodos calculate (actualmente
     * es un alias, pero a futuro, el metodo getReservedStock podria ser
     * optimizado, debiendo ser utilizado este metodo donde se requiere un
     * calculo exhaustivo del stock reservado).
     * @param quoma\products\models\Product $product
     * @param \frontend\models\Website $website
     * @param boolean $secondary
     * @return double
     */
    public function calculateReservedStock($product, $website = null, $secondary = false)
    {
        
        return $this->getReservedStock($product, $website, $secondary);
        
    }
    
}
