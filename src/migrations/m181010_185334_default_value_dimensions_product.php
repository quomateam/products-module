<?php

use yii\db\Migration;

/**
 * Class m181010_185334_default_value_dimensions_product
 */
class m181010_185334_default_value_dimensions_product extends Migration
{
    public function init()
    {
        $this->db= 'db_product';
        parent::init(); // TODO: Change the autogenerated stub
    }

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->update('product', ['weight' => 200], ['weight' => null]);
        $this->update('product', ['width' => 200], ['width' => null]);
        $this->update('product', ['height' => 30], ['height' => null]);
        $this->update('product', ['large' => 100], ['large' => null]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->update('product', ['weigth' => null]);
        $this->update('product', ['width' => null]);
        $this->update('product', ['height' => null]);
        $this->update('product', ['large' => null]);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181010_185334_default_value_dimensions_product cannot be reverted.\n";

        return false;
    }
    */
}
