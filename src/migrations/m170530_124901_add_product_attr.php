<?php

use yii\db\Migration;

class m170530_124901_add_product_attr extends Migration
{
    public function up()
    {
        $db = quoma\products\components\DbHelper::getDbName(Yii::$app->get('db_product'));
        
        $this->execute("ALTER TABLE `$db`.`product` 
            ADD COLUMN `summary` VARCHAR(255) NULL DEFAULT NULL");
    }

    public function down()
    {
        echo "m170530_124901_add_product_attr cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
