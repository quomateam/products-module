<?php

use yii\db\Migration;

/**
 * Class m180613_184721_change_product_summary
 */
class m180613_184721_change_product_summary extends Migration
{

    public function init() {
        $this->db = 'db_product';
        parent::init();
    }

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $this->alterColumn('product', 'summary', $this->string(1500));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180613_184721_change_product_summary cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180613_184721_change_product_summary cannot be reverted.\n";

        return false;
    }
    */
}
