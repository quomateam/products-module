<?php

use yii\db\Migration;

class m170110_121343_create_data_base_products extends Migration {

    public function up() {

        $db = quoma\products\components\DbHelper::getDbName(Yii::$app->get('db_product'));

        $this->execute("DROP SCHEMA IF EXISTS `$db`;");
        $this->execute("CREATE SCHEMA IF NOT EXISTS `$db` DEFAULT CHARACTER SET utf8");
        $this->execute("CREATE TABLE IF NOT EXISTS `$db`.`unit` (
            `unit_id` INT NOT NULL AUTO_INCREMENT,
            `name` VARCHAR(45) NULL,
            `type` ENUM('int','float') NULL,
            `symbol` VARCHAR(10) NULL,
            `symbol_position` ENUM('prefix','suffix') NULL,
            `code` INT NULL,
            PRIMARY KEY (`unit_id`))
          ENGINE = InnoDB;");
        $this->execute("CREATE TABLE IF NOT EXISTS `$db`.`product` (
            `product_id` INT NOT NULL AUTO_INCREMENT,
            `name` VARCHAR(100) NULL,
            `system` VARCHAR(100) NULL,
            `code` VARCHAR(45) NULL,
            `description` TEXT NULL,
            `status` ENUM('enabled','disabled') NULL,
            `balance` DOUBLE NULL,
            `secondary_balance` DOUBLE NULL,
            `create_timestamp` INT NULL,
            `update_timestamp` INT NULL,
            `unit_id` INT NOT NULL,
            `type` ENUM('service','product','plan') NULL DEFAULT 'product',
            `uid` VARCHAR(45) NULL,
            `website_id` INT NOT NULL,
            PRIMARY KEY (`product_id`),
            INDEX `fk_product_unit1_idx` (`unit_id` ASC),
            INDEX `uid` (`uid` ASC),
            CONSTRAINT `fk_product_unit1`
              FOREIGN KEY (`unit_id`)
              REFERENCES `$db`.`unit` (`unit_id`)
              ON DELETE NO ACTION
              ON UPDATE NO ACTION)
          ENGINE = InnoDB;");
        $this->execute("CREATE TABLE IF NOT EXISTS `$db`.`product_category` (
            `product_category_id` INT NOT NULL AUTO_INCREMENT,
            `name` VARCHAR(45) NULL,
            `status` ENUM('enabled','disabled') NULL,
            `system` VARCHAR(50) NULL,
            `parent_id` INT NULL,
            `website_id` INT NOT NULL,
            PRIMARY KEY (`product_category_id`),
            INDEX `fk_category_category1_idx` (`parent_id` ASC),
            CONSTRAINT `fk_category_category1`
              FOREIGN KEY (`parent_id`)
              REFERENCES `$db`.`product_category` (`product_category_id`)
              ON DELETE NO ACTION
              ON UPDATE NO ACTION)
          ENGINE = InnoDB;");
        $this->execute("CREATE TABLE IF NOT EXISTS `$db`.`product_has_category` (
            `product_id` INT NOT NULL,
            `product_category_id` INT NOT NULL,
            `order` INT NULL,
            PRIMARY KEY (`product_id`, `product_category_id`),
            INDEX `fk_product_has_category_category1_idx` (`product_category_id` ASC),
            INDEX `fk_product_has_category_product_idx` (`product_id` ASC),
            CONSTRAINT `fk_product_has_category_product`
              FOREIGN KEY (`product_id`)
              REFERENCES `$db`.`product` (`product_id`)
              ON DELETE NO ACTION
              ON UPDATE NO ACTION,
            CONSTRAINT `fk_product_has_category_category1`
              FOREIGN KEY (`product_category_id`)
              REFERENCES `$db`.`product_category` (`product_category_id`)
              ON DELETE NO ACTION
              ON UPDATE NO ACTION)
          ENGINE = InnoDB;");
        $this->execute("CREATE TABLE IF NOT EXISTS `$db`.`price_list` (
            `price_list_id` INT NOT NULL AUTO_INCREMENT,
            `name` VARCHAR(45) NULL,
            `type` VARCHAR(45) NULL DEFAULT 'sale',
            `start_date` DATE NULL,
            `end_date` DATE NULL,
            `status` VARCHAR(45) NULL,
            `default` TINYINT(1) NULL DEFAULT 0,
            `start_timestamp` INT NULL,
            `end_timestamp` INT NULL,
            `website_id` INT NOT NULL,
            PRIMARY KEY (`price_list_id`))
          ENGINE = InnoDB");
        $this->execute("CREATE TABLE IF NOT EXISTS `$db`.`product_price` (
            `product_price_id` INT NOT NULL AUTO_INCREMENT,
            `net_price` DOUBLE NULL,
            `taxes` DOUBLE NULL,
            `date` DATE NULL,
            `time` TIME NULL,
            `timestamp` INT NULL,
            `exp_timestamp` INT NULL,
            `exp_date` DATE NULL,
            `exp_time` TIME NULL,
            `update_timestamp` INT NULL,
            `status` ENUM('updated','outdated') NULL DEFAULT 'updated',
            `product_id` INT NOT NULL,
            `purchase_price` DOUBLE NULL,
            `price_list_id` INT NOT NULL,
            `website_id` INT NOT NULL,
            PRIMARY KEY (`product_price_id`),
            INDEX `fk_product_price_product1_idx` (`product_id` ASC),
            INDEX `fk_product_price_prices1_idx` (`price_list_id` ASC),
            CONSTRAINT `fk_product_price_product1`
              FOREIGN KEY (`product_id`)
              REFERENCES `$db`.`product` (`product_id`)
              ON DELETE NO ACTION
              ON UPDATE NO ACTION,
            CONSTRAINT `fk_product_price_prices1`
              FOREIGN KEY (`price_list_id`)
              REFERENCES `$db`.`price_list` (`price_list_id`)
              ON DELETE NO ACTION
              ON UPDATE NO ACTION)
          ENGINE = InnoDB;");
        $this->execute("");
    }

    public function down() {
        
        $db = quoma\products\components\DbHelper::getDbName(Yii::$app->get('db_product'));
        
        $this->execute("DROP SCHEMA IF EXISTS $db;");
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
