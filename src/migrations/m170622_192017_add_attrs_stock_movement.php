<?php

use yii\db\Migration;

class m170622_192017_add_attrs_stock_movement extends Migration
{
    public function up()
    {
        $db = quoma\products\components\DbHelper::getDbName(Yii::$app->get('db_product'));
        $main_db = quoma\products\components\DbHelper::getDbName(Yii::$app->get('db'));
        
        $this->execute("ALTER TABLE `$db`.`stock_movement` 
            ADD COLUMN `secondary_qty` DOUBLE NULL DEFAULT NULL ,
            ADD COLUMN `secondary_avaible_stock` DOUBLE NULL DEFAULT NULL , 
            ADD COLUMN `secondary_stock` DOUBLE NULL DEFAULT NULL,
            ADD COLUMN `balance` DOUBLE NULL DEFAULT NULL,
            ADD COLUMN `website_id` INT NOT NULL, 
            ADD COLUMN cart_id INT DEFAULT NULL");
        
        $this->addForeignKey('fk_stock_movement_website1', "$db.stock_movement", 'website_id', "$main_db.website", 'website_id');
        
        
    }

    public function down()
    {
      $db = quoma\products\components\DbHelper::getDbName(Yii::$app->get('db_product'));
      $this->dropColumn("`$db`.`stock_movement` ", 'secondary_qty');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
