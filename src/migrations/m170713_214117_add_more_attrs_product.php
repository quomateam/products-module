<?php

use yii\db\Migration;

class m170713_214117_add_more_attrs_product extends Migration
{
    public function up()
    {

       $db = quoma\products\components\DbHelper::getDbName(Yii::$app->get('db_product'));
        
        $this->execute("SET FOREIGN_KEY_CHECKS=0");
        
        $this->execute("ALTER TABLE `$db`.`product` 
            ADD COLUMN `secondary_unit_id` INT NOT NULL,
            ADD INDEX `fk_product_unit2_idx` (`secondary_unit_id` ASC)");
        
        
        
        $this->execute("ALTER TABLE `$db`.`product` 
            ADD CONSTRAINT `fk_product_unit2`
            FOREIGN KEY (`secondary_unit_id`)
            REFERENCES `$db`.`unit` (`unit_id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION");
        
        $this->execute("SET FOREIGN_KEY_CHECKS=1");
//        $this->addForeignKey('fk_product_unit2', "$db.product", 'secondary_unit_id', "$db.unit", 'unit_id');
    }

    public function down()
    {
        echo "m170713_214117_add_more_attrs_stock_movement cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
