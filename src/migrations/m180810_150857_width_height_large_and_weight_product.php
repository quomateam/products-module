<?php

use yii\db\Migration;

/**
 * Class m180810_150857_width_height_large_and_weight_product
 */
class m180810_150857_width_height_large_and_weight_product extends Migration
{

    public function init()
    {
        $this->db= 'db_product';
        parent::init();
    }

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('product', 'width', 'DOUBLE NULL');
        $this->addColumn('product', 'height', 'DOUBLE NULL');
        $this->addColumn('product', 'large', 'DOUBLE NULL');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('product', 'width');
        $this->dropColumn('product', 'height');
        $this->dropColumn('product', 'large');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180810_150857_width_height_large_and_weight_product cannot be reverted.\n";

        return false;
    }
    */
}
