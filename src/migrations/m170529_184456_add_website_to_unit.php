<?php

use yii\db\Migration;

class m170529_184456_add_website_to_unit extends Migration
{
    public function up()
    {
        
        $db = quoma\products\components\DbHelper::getDbName(Yii::$app->get('db_product'));
        
        $this->execute("ALTER TABLE `$db`.`unit` 
            ADD COLUMN `website_id` INT(1) NULL");

    }

    public function down()
    {
        echo "m170529_184456_add_website_to_unit cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
