<?php

use yii\db\Migration;

class m171122_174335_add_weight_product extends Migration
{
    public function up()
    {

        $db = quoma\products\components\DbHelper::getDbName(Yii::$app->get('db_product'));

        $this->execute("ALTER TABLE `$db`.`product` 
            ADD COLUMN `weight` INT NULL");

    }

    public function down()
    {
        $db = quoma\products\components\DbHelper::getDbName(Yii::$app->get('db_product'));

        $this->dropColumn("$db.product",'weight');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
