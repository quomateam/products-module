<?php

use yii\db\Migration;

class m170522_175312_add_stock_movement_table extends Migration
{
    public function up()
    {
        $db = quoma\products\components\DbHelper::getDbName(Yii::$app->get('db_product'));
        
        $this->execute("CREATE TABLE IF NOT EXISTS `$db`.`stock_movement` (
        `stock_movement_id` INT(11) NOT NULL AUTO_INCREMENT,
        `type` ENUM('in', 'out', 'r_in', 'r_out') NULL DEFAULT NULL,
        `concept` VARCHAR(255) NULL DEFAULT NULL,
        `qty` DOUBLE NULL DEFAULT NULL,
        `create_at` INT(11) NULL DEFAULT NULL,
        `date` DATE NULL DEFAULT NULL,
        `time` TIME NULL DEFAULT NULL,
        `stock` DOUBLE NULL DEFAULT NULL,
        `avaible_stock` DOUBLE NULL DEFAULT NULL,
        `product_id` INT(11) NOT NULL,
        `active` TINYINT(1) NULL DEFAULT 1,
        `expiration` DATE NULL DEFAULT NULL,
        `expiration_timestamp` INT(11) NULL DEFAULT NULL,
        PRIMARY KEY (`stock_movement_id`),
        INDEX `fk_stock_movement_product1_idx` (`product_id` ASC),
        CONSTRAINT `fk_stock_movement_product1`
          FOREIGN KEY (`product_id`)
          REFERENCES `$db`.`product` (`product_id`)
          ON DELETE NO ACTION
          ON UPDATE NO ACTION)
      ENGINE = InnoDB
      DEFAULT CHARACTER SET = utf8");

    }

    public function down()
    {
        echo "m170522_175312_add_stock_movement_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
