<?php
return [
    'code_first_on_create' => false,
    'use_price_list' => true,
    /**
     * Ubicación del bin de genbarcode
     */
    'genbarcode_location'=>'/usr/local/bin/genbarcode',
];