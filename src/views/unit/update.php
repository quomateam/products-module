<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Unit */

$this->title = quoma\products\ProductsModule::t('Update').' '. quoma\products\ProductsModule::t('Unit') .': ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => quoma\products\ProductsModule::t('Units'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->unit_id]];
$this->params['breadcrumbs'][] = quoma\products\ProductsModule::t('Update');
?>
<div class="unit-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
