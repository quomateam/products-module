<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Unit */

$this->title = quoma\products\ProductsModule::t('Create').' '. quoma\products\ProductsModule::t('Unit');
$this->params['breadcrumbs'][] = ['label' => quoma\products\ProductsModule::t('Units'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="unit-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
