<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Product */

$this->title = quoma\products\ProductsModule::t('Create').' '. quoma\products\ProductsModule::t('Product');
$this->params['breadcrumbs'][] = ['label' => quoma\products\ProductsModule::t('Products'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'price' =>$price,
        'products' => $products
    ]) ?>

</div>
