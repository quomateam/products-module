<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Product */

$this->title = quoma\products\ProductsModule::t('Update').' '. quoma\products\ProductsModule::t('Product') .': ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => quoma\products\ProductsModule::t('Products'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->product_id]];
$this->params['breadcrumbs'][] = quoma\products\ProductsModule::t('Update');
?>
<div class="product-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'price' =>$price,
        'products' => $products
    ]) ?>

</div>
