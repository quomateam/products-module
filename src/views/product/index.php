<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = quoma\products\ProductsModule::t('Products');
$this->params['breadcrumbs'][] = $this->title;

$website = $this->context->website;

?>
<div class="product-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(quoma\products\ProductsModule::t('Create') .' '.quoma\products\ProductsModule::t('Product'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'product_id',
            'name',
            'system',
            'code',
//            'description:ntext',
            // 'status',
            // 'balance',
            // 'secondary_balance',
            // 'create_timestamp:datetime',
            // 'update_timestamp:datetime',
            // 'unit_id',
            // 'type',
            // 'uid',
            [
                'format' => 'raw',
                'value' => function($model, $key, $index, $column) use ($website) {
                        return Html::a(
                            '<i class="glyphicon glyphicon-sort"></i> ' . quoma\products\ProductsModule::t('Update stock'),
                            Url::to(['/website-product/stock-movement/create','website_id' => $website->website_id, 'product_id' => $model->product_id]), 
                            [
                                'id'=>'grid-custom-button',
                                'data-pjax'=>true,
                                'action'=>Url::to(['stock-movement/create', 'id' => $model->product_id]),
                                'class'=>'button btn btn-success',
                            ]
                        );
                }
            ],    
           ['class' => 'common\components\grid\ActionColumn'],
        ],
    ]); ?>
</div>
