<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Product */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => quoma\products\ProductsModule::t('Products'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(quoma\products\ProductsModule::t('Update'), ['update', 'id' => $model->product_id], ['class' => 'btn btn-primary']) ?>
        <?php if($model->deletable):?>
            <?=
            Html::a(quoma\products\ProductsModule::t('Delete'), ['delete', 'id' => $model->product_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => quoma\products\ProductsModule::t('Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ])
            ?>
        <?php endif;?>
        <?= Html::a(
            '<i class="glyphicon glyphicon-sort"></i> Actualizar Stock',
            Url::to(['stock-movement/create','website_id' => $model->website_id,'product_id' => $model->product_id]),
            [
                'id'=>'grid-custom-button',
                'data-pjax'=>true,
                'class'=>'btn btn-success',
            ]
        ); ?>
    </p>

    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            'system',
            'code',
            'description:ntext',
            [
                'attribute' => 'status',
                'value' => Yii::t('app', ucfirst($model->status))
            ],
            'create_timestamp:date',
            'update_timestamp:date',
            [
                'attribute' => 'unit_id',
                'value' => $model->unit->name
            ],
            'weight',
            'width',
            'height',
            'large',
            [
                'label' => $model->getAttributeLabel('barcode'),
                'format' => 'image',
                'value' => \yii\helpers\Url::toRoute(['product/barcode', 'id' => $model->product_id])
            ],
            [
                'attribute' => 'relatedProducts',
                'value' => function($model){
                    if ($model->relatedProducts) {
                        $products = '';
                        foreach ($model->relatedProducts as $relatedProduct) {
                            $products .= $relatedProduct->childProduct->name.', ';
                        }
                        return $products;
                    }
                }


            ]
        ],
    ])
    ?>
    
    <h3><?= Yii::t('app','Media') ?></h3>

    <?php
    foreach($model->media as $media){
        echo quoma\media\components\view\Preview::widget(['media' => $media, 'mediaOptions' => ['style' => 'max-height: 290px;']]);
    }
    ?>

</div>
