<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use quoma\products\models\Unit;
use quoma\products\models\ProductCategory;
use yii\helpers\ArrayHelper;
use yii\bootstrap\Modal;
use yii\bootstrap\Alert;
use quoma\products\models\PriceList;
use frontend\modules\websiteConfig\models\WebsiteConfig;


$website = $model->website;
/* @var $this yii\web\View */
/* @var $model app\models\Product */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'code')->textInput(['maxlength' => 45, 'id' => 'code'])->hint(quoma\products\ProductsModule::t('This is the unique identifier of the product. Generally, it is obtained from a barcode.')) ?>

    <div class="row">
        <div class="col-lg-6">
            <?= $form->field($model, 'status')->dropDownList(['enabled' => quoma\products\ProductsModule::t('Enabled'), 'disabled' => quoma\products\ProductsModule::t('Disabled')]) ?>
        </div>
        <div class="col-lg-6">
            <?= $form->field($model, 'unit_id')->dropDownList(ArrayHelper::map(Unit::find()->where(['website_id' => $model->website_id])->all(), 'unit_id', 'name')) ?>
        </div>
    </div>



    <?php if (Yii::$app->getModule('product')->required_dimensions):?>
    <div  class="form-group <?= ($model->hasErrors('weight')) ? 'has-error' : ''?>">
        <label class="control-label"><?= $model->getAttributeLabel('weight')?></label>
        <div class="input-group">
                 <?= Html::activeTextInput($model, 'weight',['aria-describedby' => "basic-addon2",'class' => 'form-control']); ?>
                <span class="input-group-addon" id="basic-addon2">Gramos</span>
        </div>
        <?= Html::error($model, 'weight',['class' => 'help-block']); ?>
    </div>

    <?php echo $form->field($model, 'width', [
            'template' => '<div class="form-group">
                                {label}
                                <div class="input-group">
                                    {input}
                                    <span class="input-group-addon">mm</span>
                                </div>
                                {error}
                            </div>'
    ])->textInput()?>

    <?php echo $form->field($model, 'height', [
            'template' => '<div class="form-group">
                                {label}
                                <div class="input-group">
                                    {input}
                                    <span class="input-group-addon">mm</span>
                                </div>
                                {error}
                            </div>'
    ])->textInput()?>

    <?php echo $form->field($model, 'large', [
            'template' => '<div class="form-group">
                                {label}
                                <div class="input-group">
                                    {input}
                                    <span class="input-group-addon">mm</span>
                                </div>
                                {error}
                            </div>'
    ])->textInput()?>
    <?php endif;?>

    <?php if (Yii::$app->getModule('product')->required_duration):?>
        <?php echo $form->field($model, 'duration', [
            'template' => '<div class="form-group">
                                    {label}
                                    <div class="input-group">
                                        {input}
                                        <span class="input-group-addon">min</span>
                                    </div>
                                    {error}
                                </div>'
        ])->textInput()?>
    <?php endif;?>
    <hr>
    <?= $form->field($model, 'categories')->checkboxList(ArrayHelper::map(ProductCategory::getOrderedCategories([],$model->website_id), 'product_category_id', 'tabName'), ['encode' => false, 'separator' => '<br/>']) ?>

    <?php if($model->isNewRecord): ?>
        <?= $form->field($model, 'initial_stock')->textInput(['maxlength' => true]) ?> 
    <?php endif; ?>

    <?php
        echo $form->field($model, 'relatedProducts')->widget(\kartik\select2\Select2::class, [
            'data' => $products,
            'options' => ['placeholder' => \quoma\products\ProductsModule::t('Select Related Products'), 'multiple' => true],
            'pluginOptions' => [
                'allowClear' => true,
            ]
        ])
    ?>

    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title"><?= quoma\products\ProductsModule::t('Price'); ?></h3>
        </div>

        <div class="panel-body">

            <?php
            $price_list_config = WebsiteConfig::getWebsiteConfig('use_price_list',$website->website_id);
            
            $use_price_list = $price_list_config ? $price_list_config->value : false;
            
            $lists = PriceList::actives($model->website_id);
            ?>
            <?php if ($lists): ?>
                <ul>
                    <?php foreach ($lists as $list): ?>
                        <li>
                            <div class="col-md-5 col-xs-6">
                                <?php $label = $use_price_list ? $list->name : 'Precio Neto'?>
                                <?= $form->field($price, 'net_price')->textInput([
                                    'maxlength' => 20, 
                                    'id' => 'net_price', 
                                    'name' => "ProductPrice[$list->price_list_id][net_price]",
                                    'value' => ($model->isNewRecord) ? '': $model->getProductPrices()->select('net_price')->where(['price_list_id' => $list->price_list_id])->scalar()
                                    ])->label($label ); ?>
                            </div>
                            <div class="col-md-5 col-xs-6">
                                <?php $label = 'Precio Anterior'?>
                                <?= $form->field($price, 'before_price')->textInput([
                                    'maxlength' => 20,
                                    'id' => 'before_price',
                                    'name' => "ProductPrice[$list->price_list_id][before_price]",
                                    'value' => ($model->isNewRecord) ? '': $model->getProductPrices()->select('before_price')->where(['price_list_id' => $list->price_list_id])->scalar()
                                    ])->label($label ); ?>
                            </div>
                                <?= $form->field($price, 'price_list_id')->input('hidden',['value' => $list->price_list_id,'name' => "ProductPrice[$list->price_list_id][price_list_id]"])->label(false); ?>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                <?php endif; ?>

        </div>
    </div>      

    <?php  $editorOptions['editorOptions'] = [
        'toolbar' => [
            ['Sourcedialog'],
            ['PasteText', '-', 'Undo', 'Redo'],
            ['Replace', 'SelectAll'],
            ['Format', 'FontSize'],
            ['Image'],
            '/',
            ['Bold', 'Italic', 'Underline', 'TextColor', 'StrikeThrough', '-', 'Outdent', 'Indent', 'RemoveFormat',
                'Blockquote', 'HorizontalRule'],
            ['NumberedList', 'BulletedList', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight',
                'JustifyBlock'],
            ['Maximize'],
        ],
        'allowedContent' => true,
        'filebrowserUploadUrl' => NULL,
        'forcePasteAsPlainText' => true,
        'language' => Yii::$app->language,
        'inline' => false,
        'class' => 'pepepe'
        ]; ?>
    <?= $form->field($model, 'summary')->widget(\sadovojav\ckeditor\CKEditor::className(), $editorOptions) ?>
    <?= $form->field($model, 'description')->widget(\sadovojav\ckeditor\CKEditor::className(),$editorOptions) ?>
    
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title"><?= Yii::t('app','Media'); ?></h3>
        </div>
        <div class="panel-body">
            <?= quoma\media\components\widgets\Buttons::widget([
                'media' => $model->media,
                'types' => [
                    'Image', 'Youtube', 'Gif',
                ],
                'editorSelector' => '#product-description',
                'buttonOptions' => ['class' => 'btn btn-black btn-sm'],
                'searchButtonOptions' => ['class' => 'btn btn-primary btn-sm pull-right'],
                'previewOptions' => [
                    'buttonsTemplate' =>  '{insert} {delete} {custom}'
                ]
            ]) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? quoma\products\ProductsModule::t('Create') : quoma\products\ProductsModule::t('Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>


    <?php
//Modal para cargar codigo de barras
    $modal = Modal::begin([
                'header' => '<h2>' . $model->getAttributeLabel('code') . '</h2>',
                'clientEvents' => ['shown.bs.modal' => 'function(){$("#bar_code").focus();}'],
                'id' => 'm1',
    ]);

//Mostramos el error del codigo
    foreach ($model->getErrors('code') as $error)
        echo Alert::widget([
            'options' => [
                'class' => 'alert-danger',
            ],
            'body' => $error
        ]);;

    echo '<div class="form-group">';
    echo '<label>' . quoma\products\ProductsModule::t('Please, bring the reader to the barcode or type the code and press enter.') . '</label>';
    echo Html::textInput('bar_code', '', ['id' => 'bar_code', 'class' => 'form-control']);
    echo '</div>';

    echo '<div class="form-group">';
    echo '<label>' . quoma\products\ProductsModule::t('If you need a new code, please press the button "Generate new code".') . '</label><br/>';
    echo '<a class="btn btn-primary" onclick="Code.generate();">' . quoma\products\ProductsModule::t('Generate new code') . '</a>&nbsp';
    echo '<a class="btn btn-warning" onclick="Code.cancel();">' . quoma\products\ProductsModule::t('Cancel') . '</a>';
    echo '</div>';

    echo '<div class="form-group">';
    echo '<div class="hint-block">' . quoma\products\ProductsModule::t('Press "Space" to generate a new code.') . '</div>';
    echo '<div class="hint-block">' . quoma\products\ProductsModule::t('Press "Esc" to cancel.') . '</div>';
    echo '<div class="hint-block">' . quoma\products\ProductsModule::t('Press "Enter" if you type the code by hand.') . '</div>';
    echo '</div>';

    Modal::end();
    ?>

    <script type="text/javascript">

        var Code = new function () {

            this.load = function (event) {

                if (event.which == 13) {

                    if ($("#bar_code").val().trim() == '') {
                        this.generate();
                    } else {
                        $("#code").val($("#bar_code").val());
                        $('#m1').modal('hide');
                    }

                    //Borramos el codigo actual
                    $("#bar_code").val('')

                } else if (event.which == 32) {
                    event.preventDefault();
                    this.generate();
                }
            }

            this.input = function () {
                $('#m1').modal('show');
            }

            this.generate = function () {
                $("#code").val('AUTO');
                $('#m1').modal('hide');
            }

            this.cancel = function () {

                $('#m1').modal('hide');

            }
        }

    </script>

    <?php
    //Evento que permite que el lector de codigos de barra ingrese el codigo
    $js = '$("#bar_code").on("keypress",function(event){Code.load(event);});';

    //Si es necesario modificar el codigo, al hacer clic en el mismo, lo podemos hacer
    $js .= '$("#code").on("click",function(){Code.input();});';

    //Si la app esta configurada para que se deba introducir primero el codigo del producto y luego los demas datos:
    if ($model->isNewRecord)
        if (Yii::$app->getModule('website-product')->params['code_first_on_create'])
            $js .= '$("#m1").modal("show");';

    $this->registerJs($js);
    ?>
</div>
