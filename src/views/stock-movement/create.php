<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\modules\sale\models\StockMovement $model
 */

$this->title = quoma\products\ProductsModule::t('Create') . ' '. quoma\products\ProductsModule::t('Stock Movement');
$this->params['breadcrumbs'][] = ['label' => quoma\products\ProductsModule::t( 'Stock Movement'), 'url' => ['product/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="stock-movement-create">
	<!-- TO DO: ver si esta bien esta vista -->

        <h1><?= Html::encode($this->title) ?></h1>

        <h4><?= quoma\products\ProductsModule::t('Product') ?>: <?= $model->product->name; ?> #<?= $model->product->code; ?></h4>
        <hr/>
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>

        <hr/>
        <h4 class="pull-right"><?= quoma\products\ProductsModule::t('Product') ?>: <?= $model->product->name; ?> #<?= $model->product->code; ?></h4>

</div>
