<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use frontend\modules\websiteConfig\models\WebsiteConfig;

/**
 * @var yii\web\View $this
 * @var app\modules\sale\models\StockMovement $model
 */

$this->title = $model->product->name;
$this->params['breadcrumbs'][] = ['label' => quoma\products\ProductsModule::t('Stock movements'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="stock-movement-view">

    <div class="title">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>    

    <?php 
    $values = [
        'in' => '<span style="color: green;" class="glyphicon glyphicon-arrow-up"></span> '. quoma\products\ProductsModule::t('In'),
        'out' => '<span style="color: red;" class="glyphicon glyphicon-arrow-down"></span> '. quoma\products\ProductsModule::t('Out'),
        'r_in' => '<span style="color: gold;" class="glyphicon glyphicon-arrow-down"></span> '. quoma\products\ProductsModule::t('R. In'),
        'r_out' => '<span style="color: orange;" class="glyphicon glyphicon-arrow-down"></span> '. quoma\products\ProductsModule::t('R. Out')
    ];
    
    ?>
    
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'stock_movement_id',
            [
                'attribute' => 'type',
                'value' => $values[$model->type],
                'format' => ['html']
            ],
            'qtyAndUnit',
            [
                'attribute' => 'secondaryQtyAndUnit',
                'visible'=> WebsiteConfig::getWebsiteConfig('enable_secondary_stock', $model->product->website_id)->value
            ],
            'concept',
            [
                'attribute'=>'date',
                'format'=>['date','dd-MM-Y']
            ],
            'time',
        ],
    ]) ?>

</div>
