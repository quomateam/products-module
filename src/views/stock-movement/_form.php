<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use frontend\modules\websiteConfig\models\WebsiteConfig;
use quoma\products\components\StockInput;

/**
 * @var yii\web\View $this
 * @var app\modules\sale\models\StockMovement $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="stock-movement-form">

    <?php $form = ActiveForm::begin(); ?>
    

    <?= $form->field($model, 'type')->radioList(['in'=>quoma\products\ProductsModule::t('In'),'out'=>quoma\products\ProductsModule::t('Out')]) ?>

    <div class="row">
        <div class="col-sm-3">
            
            <?= $form->field($model, 'qty')->widget(\quoma\products\components\StockInput::className(),[
                'unit' => $model->product->unit
            ]) ?>

            <?php
            $config_secundary_stock = WebsiteConfig::getWebsiteConfig('enable_secondary_stock', $model->product->website_id);
            if($config_secundary_stock->value && $model->product->secondaryUnit): ?>
                <?= $form->field($model, 'secondary_qty')->widget(StockInput::className(),[
                    'unit' => $model->product->secondaryUnit
                ]) ?>
            <?php endif; ?>
            
        </div>
    </div>
    
    <?= $form->field($model, 'concept')->textInput(['maxlength' => 255]) ?>
    
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? quoma\products\ProductsModule::t( 'Create') : quoma\products\ProductsModule::t('Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
