<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use frontend\modules\websiteConfig\models\WebsiteConfig;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = quoma\products\ProductsModule::t('Stock Movements');
$this->params['breadcrumbs'][] = $this->title;

$website = $this->context->website;

?>
<div class="product-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php
    $values = [
        'in' => '<span style="color: green;" class="glyphicon glyphicon-arrow-up"></span> '. quoma\products\ProductsModule::t('In'),
        'out' => '<span style="color: red;" class="glyphicon glyphicon-arrow-down"></span> '. quoma\products\ProductsModule::t('Out'),
        'r_in' => '<span style="color: gold;" class="glyphicon glyphicon-arrow-down"></span> '. quoma\products\ProductsModule::t('R. In'),
        'r_out' => '<span style="color: orange;" class="glyphicon glyphicon-arrow-down"></span> '. quoma\products\ProductsModule::t('R. Out')
    ];

    ?>


    <!-- <p> -->
    <div class="row">
        <div class="col-sm-12">
            <form class="form-inline" role="form">

                <div class="form-group col-md-2">
                    <?= Html::activeLabel($searchModel, 'fromDate', ['class'=>'sr-only']); ?>
                    <?php
                    echo yii\jui\DatePicker::widget([
                        'language' => Yii::$app->language,
                        'model' => $searchModel,
                        'attribute' => 'fromDate',
                        'dateFormat' => 'dd-MM-yyyy',
                        'options'=>[
                            'class'=>'form-control filter dates',
                            'placeholder'=>quoma\products\ProductsModule::t('From Date')
                        ]
                    ]);
                    ?>
                    <?php //Html::activeTextInput($searchModel, 'fromDate', ['class'=>'form-control filter','placeholder'=>Yii::t('app','From Date')]); ?>
                </div>
                <div class="form-group col-md-2">
                    <?= Html::activeLabel($searchModel, 'toDate', ['class'=>'sr-only']); ?>
                    <?php
                    echo yii\jui\DatePicker::widget([
                        'language' => Yii::$app->language,
                        'model' => $searchModel,
                        'attribute' => 'toDate',
                        'dateFormat' => 'dd-MM-yyyy',
                        'options'=>[
                            'class'=>'form-control filter dates',
                            'placeholder'=>quoma\products\ProductsModule::t('To Date')
                        ]
                    ]);
                    ?>
                    <?php // Html::activeTextInput($searchModel, 'toDate', ['class'=>'form-control filter','placeholder'=>Yii::t('app','To Date')]); ?>
                </div>
                <div class="form-group col-md-2 no-padding">
                    <div onclick="Search.clearDates();" class="btn btn-warning float-right"><span class="glyphicon glyphicon-remove"></span> <?= quoma\products\ProductsModule::t('Clean dates'); ?></div>
                </div>
            </form>
        </div>
    </div>

    <div class="clearfix"><br></div>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterSelector' => '.filter',
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'type',
                'value' => function($data) use ($values){ return $values[$data->type];} ,
                'format' => ['html'],
                'filter' =>[
                    'in' => quoma\products\ProductsModule::t('In'),
                        'out' => quoma\products\ProductsModule::t('Out'),
                        'r_out' => quoma\products\ProductsModule::t('R. Out')
                    ]
            ],
            [
                    'attribute' => 'product_id',
                    'value' => function($data){ return "<a href='".Url::to(['/unique/unique-product/product/view', 'id' => $data->product_id])."'>".$data->product->name."</a>";},
                    'format' => 'raw'
            ],
            'qtyAndUnit',
            [
                'attribute' => 'secondaryQtyAndUnit',
                'visible'=> WebsiteConfig::getWebsiteConfig('enable_secondary_stock', $website->website_id)->value
            ],
            'concept',
            [
                'attribute'=>'create_at',
                'value' => function($data){ return date('d-m-Y h:i:s',$data->create_at);},
            ],
           ['class' => 'common\components\grid\ActionColumn',
               'template' => '{view}'],
        ],
    ]); ?>
</div>


<script type="text/javascript">

    var Search = new function(){

        //public
        this.windowKeypress = function(e){

            if($(":focus").length == 0 && e.which > 20 && e.which < 127) {

                autoFocus = true;

                $("#search_text").val(String.fromCharCode( e.which ));
                $("#search_text").focus();

            }

        }
        this.clear = function(){

            $.pjax({container: '#grid', url: '<?= yii\helpers\Url::toRoute(['stock-movement/graph']) ?>'});

        }

        this.clearDates = function(){

            $('.dates').val('');
            $('.dates').change();

        }

    }

</script>


