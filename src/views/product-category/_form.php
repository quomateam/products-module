<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Category */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="category-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->dropDownList([ 'enabled' => 'Enabled', 'disabled' => 'Disabled']) ?>

    <?php
    $conditions = null;
    if(!$model->isNewRecord){
        $conditions = ['<>','product_category_id',$model->product_category_id];
    }
    ?>
    
    <?= $form->field($model, 'parent_id')->dropDownList(yii\helpers\ArrayHelper::map(\quoma\products\models\ProductCategory::find()->where($conditions)->andWhere(['website_id' => $model->website_id])->all(), 'product_category_id', 'name'), ['prompt'=>Yii::t('app', 'Select parent category')]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? quoma\products\ProductsModule::t('Create') : quoma\products\ProductsModule::t('Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
