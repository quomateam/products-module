<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Category */

$this->title = quoma\products\ProductsModule::t('Create').' '. quoma\products\ProductsModule::t('Category');
$this->params['breadcrumbs'][] = ['label' => quoma\products\ProductsModule::t('Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
