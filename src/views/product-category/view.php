<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Category */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => quoma\products\ProductsModule::t('Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(quoma\products\ProductsModule::t('Update'), ['update', 'id' => $model->product_category_id], ['class' => 'btn btn-primary']) ?>
        <?php if($model->deletable):?>
            <?= Html::a(quoma\products\ProductsModule::t('Delete'), ['delete', 'id' => $model->product_category_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
        <?php endif;?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            'status',
            'system',
            'parent_id',
        ],
    ]) ?>

</div>
