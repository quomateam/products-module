<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PriceListSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = quoma\products\ProductsModule::t('Price Lists');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="price-list-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(quoma\products\ProductsModule::t('Create').' '. quoma\products\ProductsModule::t('Price List'), ['create', 'website_id' => $this->context->website ? $this->context->website->website_id : null], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'name',
            'type',
            'start_date',
            'end_date',
            // 'status',
            // 'product_id',

            ['class' => 'common\components\grid\ActionColumn'],
        ],
    ]); ?>
</div>
