<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use quoma\products\ProductsModule;

/* @var $this yii\web\View */
/* @var $model app\models\PriceList */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="price-list-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($model, 'start_date')->widget(\kartik\widgets\DatePicker::className(), [
                'pluginOptions' => [
                ]
            ]) ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'end_date')->widget(kartik\widgets\DatePicker::className(), [
                'pluginOptions' => [
                        'value' => date('d-m-Y',strtotime($model->end_date))
                ]
            ]) ?>
        </div>
    </div>
    
    <?= $form->field($model, 'status')->dropDownList( ['enabled'=>ProductsModule::t('Enabled'), 'disabled'=>ProductsModule::t('Disabled')] ) ?>

    <?= $form->field($model, 'default')->checkbox();?>
    
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? quoma\products\ProductsModule::t('Create') : quoma\products\ProductsModule::t('Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
