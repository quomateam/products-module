<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PriceList */

$this->title = quoma\products\ProductsModule::t('Update').' '. quoma\products\ProductsModule::t('Price List') .': ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => quoma\products\ProductsModule::t('Price Lists'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->price_list_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="price-list-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
