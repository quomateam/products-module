<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\PriceList */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => quoma\products\ProductsModule::t('Price Lists'), 'url' => ['index', 'website_id' => $model->website_id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="price-list-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(quoma\products\ProductsModule::t('Update'), ['update', 'id' => $model->price_list_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(quoma\products\ProductsModule::t('Delete'), ['delete', 'id' => $model->price_list_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            'type',
            'start_date',
            'end_date',
            [
                'attribute' => 'status',
                'value' => ($model->status == 'enabled')? 'Activo' : 'Inactivo'
            ],
            [
                'attribute' => 'default',
                'value' => ($model->default)? 'Si' : 'No'
            ]
        ],
    ]) ?>

</div>
