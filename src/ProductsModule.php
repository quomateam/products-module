<?php

namespace quoma\products;

use Yii;
use quoma\core\module\QuomaModule;
use quoma\core\menu\Menu;

/**
 * products module definition class
 */
class ProductsModule  extends QuomaModule
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'quoma\products\controllers';
    public $required_dimensions= true;
    public $required_duration= false;
    public $beforeAction= null;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        
        $this->loadParams();
        $this->params = array_merge($this->params, require 'params.php' );
       
        $this->registerTranslations();
        
        $this->components = [
            'stock' => [
                'class' => 'quoma\products\components\StockExpert'
            ]
        ];
    }
    
       /**
     * Registra las traducciones para el modulo.
     */
    public function registerTranslations() {
      
        \Yii::$app->i18n->translations['product'] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'basePath' => __DIR__ . '/messages',
        ];
    }
    
     /**
     * Traducciones para el modulo.
     *
     * @param $category
     * @param $message
     * @param array $params
     * @param null $language
     * @return string
     */
    public static function t($message, $params = [], $language = null, $category = '') {
        return \Yii::t(\Yii::$app->getModule('product')->getUniqueId(), $message, $params, $language);
    }
    
    /**
     * @return Menu
     */
    public function getMenu(Menu $menu)
    {
        $_menu = (new Menu(Menu::MENU_TYPE_ROOT))
            ->setName('products')
            ->setLabel(self::t('Products'))
            ->setSubItems([
                (new Menu(Menu::MENU_TYPE_ITEM))->setLabel(self::t('Products'))->setUrl(['/product/product/index']),
                (new Menu(Menu::MENU_TYPE_ITEM))->setLabel(self::t('Categories'))->setUrl(['/product/category/index']),
                (new Menu(Menu::MENU_TYPE_ITEM))->setLabel(self::t('Unit'))->setUrl(['/product/unit/index']),
                (new Menu(Menu::MENU_TYPE_ITEM))->setLabel(self::t('Price List'))->setUrl(['/product/price-list/index']),
            ])
        ;

        $menu->addItem($_menu, Menu::MENU_POSITION_LAST);
        return $_menu;
    }
    
    public function getDependencies() {
       return [
           
       ];
    }
        }
