<?php

namespace quoma\products\models;

use Yii;

/**
 * This is the model class for table "related_product".
 *
 * @property int $related_product_id
 * @property int $child_product_id
 * @property int $parent_product_id
 *
 * @property Product $childProduct
 * @property Product $parentProduct
 */
class RelatedProduct extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'related_product';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_product');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['child_product_id', 'parent_product_id'], 'integer'],
            [['child_product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['child_product_id' => 'product_id']],
            [['parent_product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['parent_product_id' => 'product_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'related_product_id' => Yii::t('app', 'Related Product ID'),
            'child_product_id' => Yii::t('app', 'Child Product ID'),
            'parent_product_id' => Yii::t('app', 'Parent Product ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChildProduct()
    {
        return $this->hasOne(Product::class, ['product_id' => 'child_product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParentProduct()
    {
        return $this->hasOne(Product::class, ['product_id' => 'parent_product_id']);
    }

}
