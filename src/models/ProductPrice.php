<?php

namespace quoma\products\models;

use Yii;
use quoma\products\ProductsModule;

/**
 * This is the model class for table "product_price".
 *
 * @property integer $product_price_id
 * @property double $net_price
 * @property double $taxes
 * @property string $date
 * @property string $time
 * @property integer $timestamp
 * @property integer $exp_timestamp
 * @property string $exp_date
 * @property string $exp_time
 * @property integer $update_timestamp
 * @property string $status
 * @property integer $product_id
 * @property double $purchase_price
 * @property integer $price_list_id
 * @property integer $website_id
 * @property double $before_price
 *
 * @property PriceList $prices
 * @property Product $product
 */
class ProductPrice extends \common\components\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_price';
    }
    
     /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb() {
        return Yii::$app->get('db_product');
    }
    
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['timestamp','update_timestamp'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['net_price', 'before_price','taxes', 'purchase_price'], 'number'],
            [['date', 'time', 'exp_date', 'exp_time', 'before_price'], 'safe'],
            [['timestamp', 'exp_timestamp', 'update_timestamp', 'product_id', 'price_list_id','website_id'], 'integer'],
            [['status'], 'string'],
            [['product_id', 'price_list_id'], 'required'],
            [['price_list_id'], 'exist', 'skipOnError' => true, 'targetClass' => PriceList::className(), 'targetAttribute' => ['price_list_id' => 'price_list_id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'product_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'product_price_id' => 'Product Price ID',
            'net_price' =>  ProductsModule::t('Net Price'),
            'taxes' => ProductsModule::t('Taxes'),
            'date' => ProductsModule::t('Date'),
            'time' => ProductsModule::t('Time'),
            'timestamp' => ProductsModule::t('Timestamp'),
            'exp_timestamp' => ProductsModule::t('Exp Timestamp'),
            'exp_date' => ProductsModule::t('Exp Date'),
            'exp_time' => ProductsModule::t('Exp Time'),
            'update_timestamp' => ProductsModule::t('Update Timestamp'),
            'status' => ProductsModule::t('Status'),
            'product_id' => 'Product ID',
            'purchase_price' => ProductsModule::t('Purchase Price'),
            'price_list_id' => ProductsModule::t('Prices ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrices()
    {
        return $this->hasOne(PriceList::className(), ['price_list_id' => 'price_list_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['product_id' => 'product_id']);
    }
    
     
    public function getFinalPrice(){
        
        return $this->net_price + $this->taxes;
        
    }
    
    public function beforeSave($insert) {
        if(parent::beforeSave($insert)){
            
            if(empty($this->exp_date)){
                $this->exp_timestamp = -1;
            }else{
                $this->exp_date = Yii::$app->formatter->asDate($this->date, 'yyyy-MM-dd');
                $this->exp_timestamp = strtotime($this->exp_date);
            }
            
            return true;
        }else{
            return false;
        }
    }
    
    public function savePrices($datas){
        
        if($datas){
            foreach ($datas as $data) {
                $model = new ProductPrice();
                $model->load($data);
            }
        }
    }
}
