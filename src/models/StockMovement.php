<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace quoma\products\models;

use Yii;
use \frontend\models\Website;
use frontend\modules\websiteConfig\models\WebsiteConfig;
use \frontend\models\Cart;
use \common\components\helpers\CheckoutHelper;

/**
 * This is the model class for table "stock_movement".
 *
 * @property int $stock_movement_id
 * @property string $type
 * @property string $concept
 * @property double $qty
 * @property int $create_at
 * @property string $date
 * @property string $time
 * @property double $stock
 * @property double $avaible_stock
 * @property int $product_id
 * @property int $active
 * @property string $expiration
 * @property string $expiration_timestamp
 * @property int $website_id
 *
 * @property Product $product
 */
class StockMovement extends \quoma\core\db\ActiveRecord 
{

    /**
     * TODO: Implementar Stock Secundario
     * * */
    const enable_secondary_stock = false;

    public function __construct($config = array()) {

        $from = get_called_class();

        if (!in_array($from, [
                    'quoma\products\models\search\StockMovementSearch',
                    'quoma\products\components\StockExpert'
                ]))
            ;

        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'stock_movement';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb() {
        return Yii::$app->get('db_product');
    }
    
    /**
     * Datos iniciales
     */
    public function init() {
        $this->active = 1;
        parent::init();
    }

    public function behaviors() {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['create_at'],
                ],
            ],
            'date' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['date'],
                ],
                'value' => function() {
                    return date('Y-m-d');
                },
            ],
            'time' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['time'],
                ],
                'value' => function() {
            return date('h:i');
        },
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        $rules = [
            [['type', 'qty'], 'required'],
            [['create_at', 'product_id','website_id'], 'integer'],
            [['date', 'time','website', 'expiration','expiration_timestamp'], 'safe'],
            [['product_id'], 'required'],
            [['concept'], 'string', 'max' => 255],
        ];

        if (!empty($this->product)) {

            $unit = $this->product->unit;

            if ($unit->type == 'int')
                $rules[] = [['qty', 'stock', 'avaible_stock'], 'integer'];
            else
                $rules[] = [['qty', 'stock', 'avaible_stock'], 'number'];

            $secondaryUnit = $this->product->secondaryUnit;

            //Secondary stock
            if (WebsiteConfig::getWebsiteConfig('enable_secondary_stock', $this->website_id)->value && $secondaryUnit) {

                if ($secondaryUnit->type == 'int')
                    $rules[] = [['secondary_qty', 'secondary_stock', 'secondary_avaible_stock'], 'integer'];
                else
                    $rules[] = [['secondary_qty', 'secondary_stock', 'secondary_avaible_stock'], 'number'];
            }
        }else {
            $rules[] = [['qty', 'stock', 'avaible_stock'], 'number'];
            if (WebsiteConfig::getWebsiteConfig('enable_secondary_stock', $this->website_id)->value
                && $this->product
                && $this->product->secondaryUnit) {
                $rules[] = [['secondary_qty', 'secondary_stock', 'secondary_avaible_stock'], 'number'];
            }
        }

        return $rules;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'stock_movement_id' => Yii::t('product', 'Stock Movement ID'),
            'type' => Yii::t('product', 'Type'),
            'concept' => Yii::t('product', 'Concept'),
            'qty' => Yii::t('product', 'Quantity'),
            'qtyAndUnit' => Yii::t('product', 'Quantity'),
            'secondary_qty' => Yii::t('product', 'Secondary Quantity'),
            'secondaryQtyAndUnit' => Yii::t('product', 'Secondary Quantity'),
            'create_at' => Yii::t('product', 'Create Timestamp'),
            'date' => Yii::t('product', 'Date'),
            'time' => Yii::t('product', 'Time'),
            'stock' => Yii::t('product', 'Stock'),
            'avaible_stock' => Yii::t('product', 'Avaible Stock'),
            'secondary_stock' => Yii::t('product', 'Secondary Stock'),
            'secondary_avaible_stock' => Yii::t('product', 'Avaible Secondary Stock'),
            'product_id' => Yii::t('product', 'Product'),
            'company' => Yii::t('product', 'Company')
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct() {
        return $this->hasOne(Product::className(), ['product_id' => 'product_id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWebsite() {
        return $this->hasOne(Website::className(), ['website_id' => 'website_id']);
    }

        /**
     * @return \yii\db\ActiveQuery
     */
    public function getCart() {
        return $this->hasOne(Cart::className(), ['cart_id' => 'cart_id']);
    }
    

    /**
     * Devuelve el color asociado al producto en formato RGB
     * @return string
     */
    public function getRgb() {

        return $this->product->rgb;
    }

    public function getDeletable() {

        return false;
    }

    /**
     * Actualizamos valores
     * @param type $insert
     * @param type $changedAttributes
     */
    public function afterSave($insert, $changedAttributes) {


        //seteo timestamp de expiración
        if ($this->type == 'r_out') {
            $expiration_config = WebsiteConfig::getWebsiteConfig('reservation_expiration', $this->website_id)->value;
            $this->expiration = date('Y-m-d', strtotime("+ $expiration_config minutes"));
            $this->expiration_timestamp = strtotime("+$expiration_config minutes", time());
            $this->updateAttributes(['expiration','expiration_timestamp']);
        }

        if ($this->type == 'in') {
            $this->product->updateAttributes([
                'balance' => $this->product->balance + $this->qty,
                'secondary_balance' => $this->product->secondary_balance + $this->secondary_qty,
            ]);
        } elseif ($this->type == 'out') {
            $this->product->updateAttributes([
                'balance' => $this->product->balance - $this->qty,
                'secondary_balance' => $this->product->secondary_balance - $this->secondary_qty,
            ]);
        }

        parent::afterSave($insert, $changedAttributes);
    }

    //Devuelve el balance a la fecha del movimiento de stock actual
    public function getBalance($website = null, $secondary = false) {
        //Stock principal o secundario?
        $qty_attr = 'qty';
        if ($secondary) {
            $qty_attr = 'secondary_qty';
        }

        $in = StockMovement::find()->where([
                    'active' => 1,
                    'product_id' => $this->product_id,
                    'type' => 'in'
                ])->andWhere('stock_movement_id<=' . $this->stock_movement_id);

        $out = StockMovement::find()->where([
            'active' => 1,
            'product_id' => $this->product_id,
            'type' => 'out'
        ]);

        if ($company) {
            $in->andWhere(['website_id' => $website->website_id]);
            $out->andWhere(['website_id' => $website->website_id]);
        }

        return $in->sum($qty_attr) - $out->sum($qty_attr);
    }

    public function getSecondaryBalance($website = null) {
        return $this->getBalance($website, true);
    }

    /**
     * Devuelve la cantidad con la unidad
     * @return string
     */
    public function getQtyAndUnit() {
        if ($this->product->unit->symbol_position == 'prefix') {
            return $this->product->unit->symbol . " $this->qty";
        } else {
            return "$this->qty" . $this->product->unit->symbol;
        }
    }

    /**
     * Devuelve la cantidad con la unidad
     * @return string
     */
    public function getSecondaryQtyAndUnit() {
        if (!$this->secondary_qty) {
            return null;
        }

        if ($this->product->secondary_unit_id && $this->product->secondaryUnit->symbol_position == 'prefix') {
            return $this->product->secondaryUnit->symbol . " $this->secondary_qty";
        } elseif ($this->product->secondary_unit_id) {
            return $this->secondary_qty . $this->product->secondaryUnit->symbol;
        }

        return null;
    }

    /**
     * Devuelve la cantidad con la unidad
     * @return string
     */
    public function getStockAndUnit() {
        if ($this->product->unit->symbol_position == 'prefix') {
            return $this->product->unit->symbol . " $this->stock";
        } else {
            return "$this->stock" . $this->product->unit->symbol;
        }
    }

    /**
     * Devuelve la cantidad con la unidad
     * @return string
     */
    public function getSecondaryStockAndUnit() {
        if (!$this->secondary_qty) {
            return null;
        }

        if ($this->product->secondary_unit_id && $this->product->secondaryUnit->symbol_position == 'prefix') {
            return $this->product->secondaryUnit->symbol . " $this->secondary_stock";
        } elseif ($this->product->secondary_unit_id) {
            return $this->secondary_stock . $this->product->secondaryUnit->symbol;
        }

        return null;
    }

    public function fields() {

        return [
            'active',
            'avaible_stock',
            'date' => function($model) {
                return Yii::$app->formatter->asDate($model->date);
            },
            'product_id',
            'qty',
            'secondary_avaible_stock',
            'secondary_qty',
            'secondary_stock',
            'stock',
            'stock_movement_id',
            'time',
            'create_at',
            'type',
            'combined_stock' => function($model) {
                $stock = $this->stockAndUnit;
                $sstock = $this->secondaryStockAndUnit;
                if ($sstock) {
                    return "$stock | $sstock";
                } else {
                    return $stock;
                }
            }
        ];
    }
    
    /**
     * Chequea consultando al modulo Checkout si el pago de una reserva de stock está aprobado.
     * Si el pago esta APROBADO registra el movimiento de salida.
     * Si el pago NO está APROBADO desactiva la reserva de stock
     * @return boolean
     */
    public function cancel()
    {
        if(is_a(\Yii::$app,'yii\console\Application')){
            echo "******************";
            echo "\n";
            echo "Stock Movement:" . $this->stock_movement_id;
            echo "\n";
            echo "Stock Movement DateTime=" . date('d-m-Y H:i:s', $this->create_at);
            echo "\n";
        }

        $cart = $this->cart;

        if (empty($cart)) {
            if(is_a(\Yii::$app,'yii\console\Application')){
                echo "No se encontró Carrito para el movimiento de stock: " . $this->stock_movement_id;
                echo "\n";
            }
            return NULL;
        }

        //chequeo que el carrito este pago
        if ($cart->isPaid()) {
            return $this->checkPayment($cart);
        }

        //busco los pagos para cancelar
        $web_payments = $cart->getWebPayments()->andWhere("status NOT IN ('canceled','timeout','error','rejected','invalid')")->all();

        if(!$web_payments){
            $this->save(['active' => 0]);
            if(is_a(\Yii::$app,'yii\console\Application')){
                echo "No se encontró Pagos para el movimiento de stock: " . $this->stock_movement_id.'. Se liberó la reserva de stock.';
                echo "\n";
            }

            return NULL;
        }
        if(is_a(\Yii::$app,'yii\console\Application')){
            echo "Payments encontrados:" . count($web_payments);
            echo "\n";
        }

        foreach ($web_payments as $web_payment) {

            if(is_a(\Yii::$app,'yii\console\Application')){
                echo "Payment uuid:" . $web_payment->uuid;
                echo "\n";
            }

            if(!$web_payment->platform_id){
                return true;
            }

            //chequeo si no ha sido pagado
            $result = CheckoutHelper::searchPayment($web_payment->uuid);

            if ($result['status'] == 'error') {
                return $result;
            }else if ($result['status'] == 'warning') {
                $cart->onError(new \yii\base\Event(['sender' => $this]));
                $web_payment->updateAttributes(['status' => 'error']);
                if(is_a(\Yii::$app,'yii\console\Application')) {
                    echo "Se canceló la venta: " . $this->cart_id;
                    echo "\n";
                }
                return true;
            }

            $payment = $result['payment'];

            //si esta pago
            if ($payment['status'] == 'paid') {
                if(is_a(\Yii::$app,'yii\console\Application')) {
                    echo "El pago fue pagado! PAGO ID:" . $payment['web_payment_id'] . " status: " . $payment['status'];
                    echo "\n";
                }

                if(is_a(\Yii::$app,'yii\console\Application')){
                    echo "Se recibió TRUE de Checkout PAGO ID: $web_payment->web_payment_id ";
                    echo "\n";
                }
                try {
                    $this->onPaid();
                } catch (Exception $exc) {
                    if(is_a(\Yii::$app,'yii\console\Application')){
                        echo "\n";
                        echo "No se pudo crear el movimiento de salida";
                    }
                    echo $exc->getTraceAsString(); return false;
                }

            } else {

                if(is_a(\Yii::$app,'yii\console\Application')) {
                    echo "EL pago no ha sido pagado PAGO ID: " . $payment['web_payment_id'] . " status: " . $payment['status'];
                    echo "\n";
                }
                //cancelo el pago en cobros
                $r = CheckoutHelper::cancelPayment($web_payment->uuid);
                if(is_a(\Yii::$app,'yii\console\Application')) {
                    echo "Cancel Payment status: " . $r['status'];
                    echo "\n";
                }

                if ($r['status'] == 'success') {
                    if(is_a(\Yii::$app,'yii\console\Application')) {
                        echo "Cancelando venta!";
                        echo "\n";
                    }
                    //Disparo el evento para desactivar la reserva de stock
                    try {
                        $cart->onError(new \yii\base\Event(['sender' => $this]));

                        if(is_a(\Yii::$app,'yii\console\Application')){
                            echo "Desactivado Stock movement:" . $this->stock_movement_id;
                            echo "\n";
                        }

                        $web_payment->updateAttributes(['status' => 'paid']);

                    } catch (Exception $exc) {
                        echo $exc->getTraceAsString();
                        if(is_a(\Yii::$app,'yii\console\Application')){
                            echo "\n";
                            echo "No se pudo registrar el error";
                        }
                        return false;
                    }

                }else {
                    if(is_a(\Yii::$app,'yii\console\Application')) {
                        echo "Se recibió NULL de Cobros PAGO ID: $web_payment->web_payment_id ";
                        echo "\n";
                    }
                }
            }
        }
        if(is_a(\Yii::$app,'yii\console\Application')){
            echo "----";
            echo "\n";
        }
        return true;
    }


    protected function onPaid($web_payment, $cart){
        //Disparo el evento para registrar el movimiento de salida
        $cart->onPaid(new \yii\base\Event(['sender' => $cart]));
        $web_payment->updateAttributes(['status' => 'paid']);
    }


    public function checkPayment($cart){

        if(is_a(\Yii::$app,'yii\console\Application')){
            echo "Carrito pagado";
            echo "\n";
        }
        try {
            //chequeo que exista un movimiento de salida para la reserva actual
            if (!$this->existOutMovement()) {
                //Disparo el evento para registrar el movimiento de salida
                $cart->onPaid(new \yii\base\Event(['sender' => $cart]));
            }
            $this->save(['active' => 0]);

        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
            if(is_a(\Yii::$app,'yii\console\Application')){
                echo "\n";
                echo "No se pudo crear el movimiento de salida";
            }
            return false;

        }
        if(is_a(\Yii::$app,'yii\console\Application')){
            echo "----";
            echo "\n";
        }
        return true;

    }
     
    /**
     * Chequea si existe un movimiento de salida activo para un movimiento de reserva
     */
//    public function existOutMovement(){
//
//        $stock_movement = StockMovement::find()->where([
//            'type' => 'out',
//            'active' => 1,
//            'qty' => $this->qty,
//            'cart_id' => $this->cart_id,
//            'website_id' => $this->website_id,
//            'product_id' => $this->product_id
//        ])->one();
//
//        if($stock_movement){
//            return true;
//        }
//
//        return false;
//
//    }
    
       /**
     * Envía mail al administrador por error en el proceso de cancelación
     * @param type $web_payment
     * @param type $reservation
     * @param type $error
     */
    public function sendMail($web_payment, $cart, $error)
    {
        try {
            
            $body = 'Existe un carrito con un pago dudoso. Pedimos por favor se verifique el siguiente pago:\n\n';
            $body .= "Pago id: $web_payment->web_payment_id, Nro de operación: $web_payment->uuid, Stock Movement ID: $cart->cart_id, Error: $error\n\n";

            Yii::$app->mailer->compose()
                    ->setFrom(Yii::$app->params['adminEmail'])
                    ->setTo(Yii::$app->params['adminEmail'])
                    ->setSubject('URGENTE! Ocurrió algún error en un pago.')
                    ->setTextBody($body)
                    ->send();
        
        } catch (Exception $exc) {
           echo $exc->getTraceAsString();
        }
    }
    
    public static function reserveStock($product_id, $qty, $website_id)
    {
        $movement = new StockMovement([
           'type' => 'r_out',
            'qty' => $qty,
            'product_id' => $product_id,
            'website_id' => $website_id
        ]);

        return $movement->save();
    }

}
