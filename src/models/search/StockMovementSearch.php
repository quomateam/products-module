<?php

namespace  quoma\products\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use quoma\products\models\StockMovement;

/**
 * StockMovementSearch represents the model behind the search form about `app\modules\sale\models\StockMovement`.
 */
class StockMovementSearch extends StockMovement
{
    
    //Solo para graph search
    public $toDate;
    public $fromDate;
    public $chartType = 'Line';
    
    public function init()
    {
        
        parent::init();
        
    }
    
    public function rules()
    {
        return [
            [['stock_movement_id', 'website_id','cart_id'], 'integer'],
            [['type', 'concept', 'date', 'product_id'], 'safe'],
            [['qty', 'stock', 'avaible_stock'], 'number'],
            [['toDate', 'fromDate'], 'date', 'format' => 'php:d-m-Y'],
            [['toDate', 'fromDate'], 'default', 'value'=>null],
            [['chartType'], 'in', 'range'=>['Line','Bar','Radar']]
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = StockMovement::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['stock_movement_id'=>SORT_DESC]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {

            return $dataProvider;
        }
        
        $query->andFilterWhere([
            'stock_movement_id' => $this->stock_movement_id,
            'qty' => $this->qty,
            'stock_movement.type' => $this->type,
            'create_at' => $this->create_at,
            'date' => $this->date,
            'stock' => $this->stock,
            'avaible_stock' => $this->avaible_stock,
            'stock_movement.website_id' => $this->website_id,
            'cart_id' => $this->cart_id,
            'active' => $this->active
        ]);

        if($this->product_id){
            $query->joinWith(['product']);
            $query->andFilterWhere(['like','product.name', $this->product_id]);
        }

        $query->andFilterWhere(['like', 'concept', $this->concept]);
        
        // Para optimizar la busqueda, buscamos por timestamp
        if(!empty($this->fromDate)){
            $query->andWhere ('create_at>='.(int)strtotime($this->fromDate));
        }
        
        // En el caso de la fecha de fin debemos compararla como menor que el timestamp del dia siguiente para cubrir todo el dia
        if(!empty($this->toDate)){
            $toDate = (int)strtotime($this->toDate.' +1day');
            $query->andWhere ('create_at<'.$toDate);
        }

        return $dataProvider;
    }
    

}
