<?php

namespace quoma\products\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use quoma\products\models\Product;

/**
 * ProductSearch represents the model behind the search form about `app\models\Product`.
 */
class ProductSearch extends Product {

    public $categories;
    public $text;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['product_id', 'create_timestamp', 'update_timestamp', 'unit_id'], 'integer'],
            [['name', 'system', 'code', 'description', 'status', 'type', 'uid', 'categories', 'text', 'website_id'], 'safe'],
            [['balance', 'secondary_balance'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = Product::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        /**if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }**/


        // grid filtering conditions
        $query->andFilterWhere([
            'product_id' => $this->product_id,
            'balance' => $this->balance,
            'secondary_balance' => $this->secondary_balance,
            'create_timestamp' => $this->create_timestamp,
            'update_timestamp' => $this->update_timestamp,
            'unit_id' => $this->unit_id,
            'product.website_id' => $this->website_id,
        ]);

        $query->andFilterWhere(['like', 'product.name', $this->name])
                ->andFilterWhere(['like', 'product.system', $this->system])
                ->andFilterWhere(['like', 'code', $this->code])
                ->andFilterWhere(['like', 'description', $this->description])
                ->andFilterWhere(['product.status' => $this->status])
                ->andFilterWhere(['like', 'type', $this->type])
                ->andFilterWhere(['like', 'uid', $this->uid]);


        $query->joinWith(['categories' => function($query) {
            if ($this->categories && !(count($this->categories) == 1 && empty(array_values($this->categories)[0]))) {
                $query->andFilterWhere(['in', 'product_category.product_category_id', $this->categories]);
            }

            return $query;
        }]);

        if ($this->text != '') {
            $query->andWhere(['OR', ['like', 'product.name', $this->text], ['like', 'product_category.name', $this->text]]);
            //$query->orWhere(['like', 'product_category.name', $this->text]);
        }

        $query->distinct();

        return $dataProvider;
    }

}
        