<?php

namespace quoma\products\models;

use Yii;

/**
 * This is the model class for table "category".
 *
 * @property integer $product_category_id
 * @property string $name
 * @property string $status
 * @property string $system
 * @property integer $parent_id
 * @property integer $website_id
 *
 * @property ProductCategory $parent
 * @property ProductCategory[] $categories
 * @property ProductHasCategory[] $productHasCategories
 * @property Product[] $products
 */
class ProductCategory extends \common\components\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'product_category';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb() {
        return Yii::$app->get('db_product');
    }

    public function behaviors() {
        return [
            'slug' => [
                'class' => \yii\behaviors\SluggableBehavior::className(),
                'slugAttribute' => 'system',
                'attribute' => 'name',
                'ensureUnique' => true,
                'immutable' => true
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['status'], 'string'],
            [['parent_id','website_id'], 'integer'],
            [['name'], 'string', 'max' => 45],
            [['system'], 'string', 'max' => 50],
            [['name','status'], 'required'],
            [['parent_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProductCategory::className(), 'targetAttribute' => ['parent_id' => 'product_category_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'product_category_id' => 'ProductCategory ID',
            'name' => \quoma\products\ProductsModule::t('Name'),
            'status' => \quoma\products\ProductsModule::t('Status'),
            'system' => \quoma\products\ProductsModule::t('System'),
            'parent_id' => \quoma\products\ProductsModule::t('Parent'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent() {
        return $this->hasOne(ProductCategory::className(), ['product_category_id' => 'parent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories() {
        return $this->hasMany(ProductCategory::className(), ['parent_id' => 'product_category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductHasCategories() {
        return $this->hasMany(ProductHasCategory::className(), ['product_category_id' => 'product_category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts() {
        return $this->hasMany(Product::className(), ['product_id' => 'product_id'])->viaTable('product_has_category', ['product_category_id' => 'product_category_id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWebsite() {
        return $this->hasOne(\common\models\Website::className(), ['website_id' => 'website_id']);
    }

    /**
     * Recursivo. Devuelve un arraglo ordenado de categorias padre > hijos
     * @param ProductCategory $parents
     * @return array Arreglo jerarquizado de categorias
     */
    public static function getOrderedCategories($parents = [], $website_id = NULL) {

        //Si el parametro no es un array, tiro exception
        if (!is_array($parents)) {
            throw new \InvalidArgumentException('Invalid argument. Expected: Array.');
        }

        $nestedCategories = array();

        //Si el padre esta vació traigo todos los padres
        if (empty($parents)) {
            $query = ProductCategory::find()->where('parent_id IS NULL');

            if($website_id){
                $query->andWhere(['website_id' => $website_id]);
            }
            //Categorias padres absolutas
            $parents = $query->all();
        }

        //Recorremos el arraglo de padres para construir el arbol de cada uno
        foreach ($parents as $parent) {

            //si no tiene hijos agrego el padre al array
            if (empty($parent->categories))
                $nestedCategories[] = $parent;
            else {
                //sino agrego el padre y llama nuevamente a la función para que agregue a sus hijos.
                $nestedCategories[] = $parent;
                $nestedCategories = array_merge($nestedCategories, self::getOrderedCategories($parent->categories));
            }
        }

        return $nestedCategories;
    }

    /**
     * Recursivo. Devuelve un arraglo ordenado de categorias padre > hijos
     * @param ProductCategory $parents
     * @return array Arreglo jerarquizado de categorias
     */
    public static function getNestedCategories($parents = []) {

        //Si el parametro no es un array, tiro exception
        if (!is_array($parents)) {
            throw new \InvalidArgumentException('Invalid argument. Expected: Array.');
        }

        $nestedCategories = array();

        //Si el padre esta vació traigo todos los padres
        if (empty($parents)) {
            //Categorias padres absolutas
            $parents = ProductCategory::find()->where('parent_id IS NULL')->all();
        }

        //Recorremos el arraglo de padres para construir el arbol de cada uno
        foreach ($parents as $parent) {

            //si no tiene hijos agrego el padre al array
            if (empty($parent->categories))
                $nestedCategories[] = [$parent];
            else {
                //sino agrego el padre y llama nuevamente a la función para que agregue a sus hijos.
                $nestedCategories[] = [
                    $parent,
                    'children' => array_merge($nestedCategories, self::getNestedCategories($parent->categories))
                ];
            }
        }

        return $nestedCategories;
    }

    /**
     * Devuelve el nombre de un label con sangría para indicar la jerarquia que tiene en el arbol visualmente
     * @return string #
     */
    public function getTabName() {
        //Establece una sangría por nivel de Padres de Categoria
        $indent = '&nbsp;&nbsp;&nbsp;&nbsp;';

        // igualo $parent al padre  
        $parent = $this->parent;
        // si $parent esta vació es decir no tiene padre el name queda igual SINO le agrega una sangría con $indent
        $name = empty($parent) ? $this->name : $indent . $this->name;

        if (!empty($parent)) {
            do {
                if ($parent = $parent->parent)
                    $name = $indent . $name; //le agrego otra sangría
            }
            while (!empty($parent)); // mientras tenga padre
        }

        return $name;
    }
    
    
    /**
     * Busca la categoria. Si no la encuentra, lanza error
     * @param type $slug
     * @return type
     * @throws \yii\web\HttpException
     */
    public static function findSlug($slug){
        
        $category = self::find()->where(['system' => $slug])->one();
        if($category === null){
            throw new \yii\web\HttpException(500, 'Category "'.$slug. '" must be created.');
        }
        return $category;
        
    }

    public function getDeletable() {

        if ($this->getCategories()->exists()) {
            return false;
        }

        if ($this->getProducts()->exists()) {
            return false;
        }

        return true;
    }


    /**
     * Devuelve la url para ver el articulo en el frontend
     * @return string
     */
    public function getUrl()
    {
        return \yii\helpers\Url::to(['/website-product/product/category', 'category' =>  $this->system ], true);

    }

}
