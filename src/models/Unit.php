<?php

namespace quoma\products\models;

use function GuzzleHttp\debug_resource;
use Yii;

/**
 * This is the model class for table "unit".
 *
 * @property integer $unit_id
 * @property string $name
 * @property string $type
 * @property string $symbol
 * @property string $symbol_position
 * @property integer $code
 * @property integer $website_id
 *
 * @property Product[] $products
 */
class Unit extends \quoma\core\db\ActiveRecord 
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'unit';
    }
    
     /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb() {
        return Yii::$app->get('db_product');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type','symbol_position'],'required'],
            [['type', 'symbol_position'], 'string'],
            [['code','website_id'], 'integer'],
            [['name'], 'string', 'max' => 45],
            [['symbol'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'unit_id' => \quoma\products\ProductsModule::t('Unit'),
            'name' => \quoma\products\ProductsModule::t('Name'),
            'type' => \quoma\products\ProductsModule::t('Type'),
            'symbol' => \quoma\products\ProductsModule::t('Symbol'),
            'symbol_position' => \quoma\products\ProductsModule::t('Symbol Position'),
            'code' => \quoma\products\ProductsModule::t('Code'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['unit_id' => 'unit_id']);
    }

      /**
     * @return \yii\db\ActiveQuery
     */
    public function getWebsite() {
        return $this->hasOne(\common\models\Website::className(), ['website_id' => 'website_id']);
    }

    /**
     * Crea una unidad por defecto
     * @return bool
     */
    public static function createDefaultUnit($website_id){

        //chequeo si ya existe una unidad creda para este sitio no la creo de nuevo
        if(Unit::find()->where(['website_id' => $website_id])->one()){
           return true;
        }

        $default_unit = new Unit();
        $default_unit->name = 'Unidades';
        $default_unit->type = 'int';
        $default_unit->symbol = 'u.';
        $default_unit->symbol_position = 'suffix';
        $default_unit->code = 1;
        $default_unit->website_id = $website_id;

        if($default_unit->save()){
            \Yii::$app->getSession()->setFlash('success', 'Se generó una unidad de producto por defecto.');
            return true;
        }
        return false;
    }
}
