<?php

namespace quoma\products\models;

use Yii;
use quoma\products\ProductsModule;

/**
 * This is the model class for table "price_list".
 *
 * @property integer $price_list_id
 * @property string $name
 * @property string $type
 * @property string $start_date
 * @property string $end_date
 * @property string $status
 * @property integer $default
 * @property integer $start_timestamp
 * @property integer $end_timestamp
 * @property integer $website_id
 *
 * @property Product $product
 * @property ProductPrice[] $productPrices
 */
class PriceList extends \common\components\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'price_list';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb() {
        return Yii::$app->get('db_product');
    }
    

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['name','start_date','end_date'], 'required'],
            [['start_date', 'end_date','default','website_id'], 'safe'],
            [['name', 'type', 'status'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'price_list_id' => 'Price List ID',
            'name' => ProductsModule::t('Name'),
            'type' => ProductsModule::t('Type'),
            'start_date' => ProductsModule::t('Start Date'),
            'end_date' => ProductsModule::t('End Date'),
            'status' => ProductsModule::t('Status'),
            'default' => ProductsModule::t('Default price list'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductPrices() {
        return $this->hasMany(ProductPrice::className(), ['price_list_id' => 'price_list_id']);
    }
    
     /**
     * @return \yii\db\ActiveQuery
     */
    public function getWebsite() {
        return $this->hasOne(\common\models\Website::className(), ['website_id' => 'website_id']);
    }

    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {

            $this->start_date = date('Y-m-d', strtotime($this->start_date));
            $this->end_date = date('Y-m-d', strtotime($this->end_date));
            $this->start_timestamp = strtotime($this->start_date);
            $this->end_timestamp = strtotime($this->end_date);
            return true;
        } else {
            return false;
        }
    }
    
    public static function actives($website_id)
    {
        $condition = "status='enabled' "
                . "AND start_timestamp <= ".  strtotime(date('Y-m-d')) ." "
                . "AND end_timestamp >= " . strtotime(date('Y-m-d')). " "
                . "AND website_id = $website_id";
        
        $lists = self::find()->where($condition)->all();
        
        if(!$lists){
             throw new \yii\web\NotFoundHttpException('Debe crear al menos una lista de precio.');
        }
        return $lists;
    }
    
    public static function defaultList()
    {
        $condition = "status='enabled' AND start_timestamp >= ".  strtotime(date('Y-m-d')) ." AND end_timestamp <= " . strtotime(date('Y-m-d'));
        return self::find()->where($condition)->one();
    }

    public static function createDefaultList($website_id){

        //chequeo si ya existe una lista de precios creda para este sitio no la creo de nuevo
        if(PriceList::find()->where(['website_id' => $website_id])->one()){
            return true;
        }

        $price_list = new PriceList();
        $price_list->name = 'Default List';
        $price_list->start_date = date('Y-m-d');
        $price_list->end_date = date('Y-m-d', strtotime('+1 years'));
        $price_list->status = 'enabled';
        $price_list->default = TRUE;
        $price_list->website_id = $website_id;

        if($price_list->save()){
            \Yii::$app->getSession()->setFlash('success', 'Se generó una unidad de lista de precio por defecto.');
           return true;
        }

        return false;
    }
    
    public function getDeletable(){
        
        if($this->getProductPrices()->exists()){
            return false;
        }
        
              
        return true;
        
    }
            

}
