<?php

namespace quoma\products\models;

use Yii;
use quoma\products\ProductsModule;
use quoma\products\models\Service;
use frontend\modules\websiteConfig\models\WebsiteConfig;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "product".
 *
 * @property integer $product_id
 * @property string $name
 * @property string $system
 * @property string $code
 * @property string $description
 * @property string $summary
 * @property string $status
 * @property double $balance
 * @property double $secondary_balance
 * @property integer $create_timestamp
 * @property integer $update_timestamp
 * @property integer $unit_id
 * @property string $type
 * @property string $uid
 * @property integer $website_id
 * @property float $weight
 * @property float $width
 * @property float $height
 * @property float $large
 * @property integer $duration
 *
 * @property PriceList[] $priceLists
 * @property Unit $unit
 * @property ProductHasCategory[] $productHasCategories
 * @property ProductCategory[] $categories
 * @property ProductPrice[] $productPrices
 * @property RelatedProduct[] $relatedProducts
 */
class Product extends \quoma\core\db\ActiveRecord
{
    public $price_list_id;
    private $_categories;
    const TYPE = 'product';
    
    public $initial_stock;
    public $initial_secondary_stock;
    
    //Para utilizar al mostrar stock:
    public $stockWebsite = null;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product';
    }
    
     /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb() {
        return Yii::$app->get('db_product');
    }
    
     /**
     * Inicializa stock en 0
     */
    public function init() {
        parent::init();
        $this->initial_stock = 0;
        $this->initial_secondary_stock = 0;
        
        $this->status = 'enabled';
    }
    
    
    /**
     * Instancia un nuevo objeto de acuerdo al tipo. El objeto puede ser:
     *  Service.
     *  Plan.
     *  Product.
     * @param array $row
     * @return \app\modules\sale\models\Plan|\app\modules\sale\models\Service|\self
     */
    public static function instantiate($row)
    {
        switch ($row['type']) {
            case Service::TYPE:
                return new Service();
            default:
               return new self;
        }
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {

        $rules = [
            [['description', 'type'], 'string'],
            [['unit_id','website_id'], 'integer'],
//            [['taxRates'], 'safe'],
            [['status'], 'in', 'range'=>['enabled','disabled']],

            [['name'], 'string', 'max' => 100],
            [['code'], 'string', 'max' => 45],
            [['summary'], 'string', 'max' => 1500],
            [['code'], 'unique'],
            [['weight', 'duration'], 'integer'],
            [['large', 'width', 'height'], 'number'],
            [['categories','initial_stock', 'duration', 'relatedProducts'], 'safe'],
            [['unit_id'], 'exist', 'skipOnError' => true, 'targetClass' => Unit::className(), 'targetAttribute' => ['unit_id' => 'unit_id']],
        ];

        if (Yii::$app->getModule('product')->required_dimensions){
            $rules[]= [['unit_id','name', 'status', 'width', 'height', 'large', 'weight', 'website_id'], 'required'];
        }else{
            $rules[]= [['unit_id','name', 'status', 'website_id'], 'required'];
        }

        if (Yii::$app->getModule('product')->required_duration !== false){
            $rules[]= [['duration'], 'required'];
        }
        return $rules;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'product_id' => ProductsModule::t('Product'),
            'name' => ProductsModule::t('Name'),
            'system' => ProductsModule::t('System'),
            'summary' => ProductsModule::t('Summary'),
            'code' => ProductsModule::t('Code'),
            'description' => ProductsModule::t('Description'),
            'status' => ProductsModule::t('Status'),
            'balance' => ProductsModule::t('Balance'),
            'secondary_balance' => ProductsModule::t('Secondary Balance'),
            'create_timestamp' => ProductsModule::t('Create Timestamp'),
            'update_timestamp' => ProductsModule::t('Update Timestamp'),
            'unit_id' => ProductsModule::t('Unit'),
            'type' => ProductsModule::t('Type'),
            'uid' => ProductsModule::t('Uid'),
            'categories' => ProductsModule::t('Categories'),
            'initial_stock' => ProductsModule::t('Initial Stock'),
            'secondary_qty' => ProductsModule::t('Secondary Quantity'),
            'weight' => ProductsModule::t('Weight'),
            'duration' => ProductsModule::t('Duration'),
            'relatedProducts' => ProductsModule::t('Related Products'),
        ];
    }
    
     public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['create_timestamp','update_timestamp'],
                    \yii\db\ActiveRecord::EVENT_BEFORE_UPDATE => 'update_timestamp',
                ],
            ],
            'slug'=>[
                'class' => \yii\behaviors\SluggableBehavior::className(),
                'slugAttribute' => 'system',
                'attribute' => 'name',
                'ensureUnique'=>true
            ],
            /**
             * Este behavior es actualizado al setear el grupo del modelo o en 
             * afterFind(), pero lo necesitamos al momento de inicializar un
             * nuevo objeto de articulo para poder mostrar la caja de búsqueda
             * de multimedia
             */
            'media' => [
                'class' => \quoma\media\behaviors\MediaBehavior::className(),
                'modelClass' => Product::className(),
                'mediaTypes' => [
                    'Image',
//                    'Youtube',
//                    'Gif',
//                    'Map',
                ]
            ],
        ];
    }
    
    /**
     * Antes de guardar verificamos si el codigo del producto debe ser generado. 
     * En caso de que el valor de code sea AUTO, el codigo se genera utilizando
     * la funcion uniqid().
     * @param type $insert
     * @return boolean
     */
    public function beforeSave($insert) 
    {
        if (parent::beforeSave($insert)) {
            
            if($this->code == 'AUTO'){
                $this->code = uniqid();
            }
            
            if($this->isNewRecord){
                if($this->secondary_unit_id == NULL){
                   $this->secondary_unit_id = $this->unit_id; 
                }
            }
            
            return true;
        } else {
            return false;
        }
    }
    
     public function afterSave($insert, $changedAttributes) 
    {
        
        parent::afterSave($insert, $changedAttributes);
        
        /**
         * Si es un nuevo producto y se definio stock inicial, inicializamos el stock
         */
        if($insert){
            $this->initStock( $this->initial_stock, $this->initial_secondary_stock );
        }
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStockMovements()
    {
        return $this->hasMany(StockMovement::className(), ['product_id' => 'product_id']);
    }
    
        
    /**
     * Inicializa el stock de un producto, creando un movimiento de stock de entrada
     * inicial por la cantidad indicado por $initialStock. Si el stock inicial es 0,
     * no genera movimiento de entrada.
     * @param int $initialStock
     */
    private function initStock($initialStock, $initialSecondaryStock = 0)
    {
        
        if($initialStock != null || $initialStock != 0){
            
            $mov =  \Yii::$app->getModule('product')->stock->createMoveIn();
            $mov->scenario = 'initial';
            $mov->website_id = $this->website_id;
            $mov->concept = Yii::t('app', 'Initial stock');
            $mov->qty = $initialStock;
            $mov->secondary_qty = $initialSecondaryStock;
            $mov->type = 'in';

            $this->link('stockMovements',$mov);

        }
        
    }
    
    /**
     * Establece los atributos que deben ser utilizados al momento de convertir
     * el objeto a array
     * @return type
     */
    public function fields() 
    {
        Yii::$app->formatter->decimalSeparator = '.';
        Yii::$app->formatter->thousandSeparator = '';

        return [
            'uid',
            'stock'=>function($model, $field){
                return $model->getStock();
            },
            'secondary_stock'=>function($model, $field){
                return $model->getSecondaryStock($this->website);
            },
            'combined_stock'=>function($model){
                $secondaryStock = $model->getSecondaryStock($this->website, true);
                if($secondaryStock){
                    return $model->getStock( true).' | '.$secondaryStock;
                }else{
                    return $model->getStock(true);
                }
            },
                    
            'avaible_stock'=>function($model, $field){
                return $model->getAvaibleStock();
            },
            'avaible_secondary_stock'=>function($model, $field){
                return $model->getSecondaryAvaibleStock($this->website);
            },
            'avaible_combined_stock'=>function($model){
                $secondaryStock = $model->getSecondaryAvaibleStock($this->website, true);
                if($secondaryStock){
                    return $model->getAvaibleStock(true).' | '.$secondaryStock;
                }else{
                    return $model->getAvaibleStock(true);
                }
            },
                    
            'inStock'=>function($model, $field){
                return $model->getInStock($this->website_id);
            },
            'code',
            'create_timestamp',
            'description',
            'name',
            'product_id',
            'status',
            'system',
//            'taxRates',
            'unit_id',
            'unit',
            'netPrice'=>function($model, $field){
                return $model->netPrice ? Yii::$app->formatter->asDecimal($model->netPrice, 2) : '';
            },
            'finalPrice'=>function($model, $field){
                return $model->finalPrice ? Yii::$app->formatter->asDecimal($model->finalPrice, 2) : '';
            },
            'priceDate'=>function($model){
                return $model->activePrice ? Yii::$app->formatter->asDate($model->activePrice->date) : null;
            },
            'categories',
            'media',
            'poster' => function($model){
                return $model->getImage();
            },
        ];
    }
    
    /**
     * Atributo virtual inStock, que permite determinar si el producto actual 
     * se encuentra en stock.
     * @return type
     */
    public function getInStock($website = null)
    {
        return $this->getAvaibleStock() > 0 || !WebsiteConfig::getWebsiteConfig('strict_stock', $website)->value;
        
    }
    
     /**
     * Es stockable??
     * @return boolean
     */
    public function getStockable(){
        return true;
    }
    
    /**
     * Stock por sitio (existencias; no tiene en cuenta reservas)
     * @param type $website
     * @return float
     * @throws \yii\web\HttpException
     */
    public function getStock($symbol = false)
    {
        
        $value = Yii::$app->getModule('product')->stock->getStock($this, $this->website);
        
        if($symbol == false){
            return $value;
    }
    
        if($this->unit->symbol_position == 'prefix'){
            return $this->unit->symbol . " $value";
        }else{
            return "$value". $this->unit->symbol;
        }
        
    }
    
    /**
     * Stock por empresa (existencias; no tiene en cuenta reservas)
     * @param type $website
     * @return float
     * @throws \yii\web\HttpException
     */
    public function getSecondaryStock($website = null, $symbol = false)
    {
        if(!$this->secondaryUnit){
            return null;
        }
        
        $value = Yii::$app->getModule('product')->stock->getStock($this, $website, true);
        
        if($symbol == false){
            return $value;
        }
        
        if($this->secondaryUnit->symbol_position == 'prefix'){
            return $this->secondaryUnit->symbol . " $value";
        }else{
            return $value . $this->secondaryUnit->symbol;
        }
        
    }
    
    /**
     * Stock disponible (existencias - reservado),
     * @param type $website
     * @return float
     * @throws \yii\web\HttpException
     */
    public function getAvaibleStock($symbol = false)
    {
        
        //Last move
        $lm = StockMovement::find()->orderBy(['stock_movement_id' => SORT_DESC])->where([
            'active'=>1, 
            'product_id' => $this->product_id,
            'type'=>['in']
        ])->andWhere(['website_id'=>$this->website_id])->cache(1)->one();
        
        //si no existe un movimiento de stock inicial lo creo
        if(!$lm){
            $mov =  \Yii::$app->getModule('product')->stock->createMoveIn();
            $mov->scenario = 'initial';
            $mov->website_id = $this->website_id;
            $mov->concept = Yii::t('app', 'Initial stock');
            $mov->qty = 0;
            $mov->secondary_qty = 0;
            $mov->type = 'in';

            $this->link('stockMovements',$mov);           
        }
        
        $value = Yii::$app->getModule('product')->stock->getAvaibleStock($this, $this->website);
       
        if($symbol == false){
            return $value;
        }
        
        if($this->unit->symbol_position == 'prefix'){
            return $this->unit->symbol . " $value";
        }else{
            return "$value". $this->unit->symbol;
        }
        
    }
    
    /**
     * Stock disponible (existencias - reservado), por empresa
     * @param type $$website
     * @return float
     * @throws \yii\web\HttpException
     */
    public function getSecondaryAvaibleStock($website = null, $symbol = false)
    {
        
        if(!$this->secondaryUnit){
            return null;
        }
        
        $value = Yii::$app->getModule('product')->stock->getAvaibleStock($this, $website, true);
        
        if($symbol == false){
            return $value;
        }
        
        if($this->secondaryUnit->symbol_position == 'prefix'){
            return $this->secondaryUnit->symbol . " $value";
        }else{
            return $value . $this->secondaryUnit->symbol;
        }
        
    }
    
    /**
     * Stock reservado, por empresa
     * @param type $website
     * @return float
     * @throws \yii\web\HttpException
     */
    public function getReservedStock($website = null, $symbol = false)
    {

        if($website === null){
            $website = \frontend\models\Website::getCurrent();
        }

        if(!$website){
            throw new \yii\web\HttpException(500, 'Website not defined.');
        }
        
        $value = Yii::$app->getModule('product')->stock->getReservedStock($this, $website);
        
        if($symbol == false){
            return $value;
        }
        
        if($this->unit->symbol_position == 'prefix'){
            return $this->unit->symbol . " $value";
        }else{
            return "$value". $this->unit->symbol;
        }
        
    }
    
    /**
     * Stock reservado, por empresa
     * @param type $website
     * @return float
     * @throws \yii\web\HttpException
     */
    public function getSecondaryReservedStock($website = null, $symbol = false)
    {
        
         if($website === null){
            $website = \frontend\models\Website::getCurrent();
        }
        
        if(!$website){
            throw new \yii\web\HttpException(500, 'Website not defined.');
        }
        
        $value = Yii::$app->getModule('product')->stock->getReservedStock($this, $website, true);
        
        if($symbol == false){
            return $value;
        }
        
        if($this->secondaryUnit){
            if($this->secondaryUnit->symbol_position == 'prefix'){
                return $this->secondaryUnit->symbol . " $value";
            }else{
                return $value . $this->secondaryUnit->symbol;
            }
        }
        
    }
    /**
     * Genera un codigo de barras en formato jph
     * @return type
     */
    public function getBarcode()
    {
        $barcode = new Barcode();
        $barcode->setGenbarcodeLocation(Yii::$app->getModule('product')->params['genbarcode_location']);
        $barcode->setMode(Barcode::MODE_PNG);

        $headers = array(
            'Content-Type' => 'image/png',
            'Content-Disposition' => 'inline; filename="'.$this->code.'.png"'
        );

       return $barcode->outputImage($this->code);
    }
    
    public function getImage()
    {
        $images = $this->getMedia('Image');
        if($images){
            return $images[0];
        }
        
        return null;
        
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPriceLists()
    {
        return $this->hasMany(PriceList::className(), ['product_id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnit()
    {
        return $this->hasOne(Unit::className(), ['unit_id' => 'unit_id'])->cache(1);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductHasCategories()
    {
        return $this->hasMany(ProductHasCategory::className(), ['product_id' => 'product_id']);
    }
    
     /**
     * @return \yii\db\ActiveQuery
     */
    public function getWebsite() {
        return $this->hasOne(\common\models\Website::className(), ['website_id' => 'website_id'])->cache(1);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasMany(ProductCategory::className(), ['product_category_id' => 'product_category_id'])->viaTable('product_has_category', ['product_id' => 'product_id']);
    }

    /**
     * Productos relacionados
     * @return RelatedProduct[]
     */
    public function getRelatedProducts()
    {
        return $this->hasMany(RelatedProduct::class, ['parent_product_id' => 'product_id']);
    }


    public function setRelatedProducts($products)
    {
        if ($this->relatedProducts) {
            $this->unlinkAll('relatedProducts', true);
        }

        $save = function () use ($products) {
            if  ($products) {
                foreach ($products as $product) {
                    $relatedProduct = new RelatedProduct([
                        'parent_product_id' => $this->product_id,
                        'child_product_id' => $product
                    ]);

                    $relatedProduct->save();
                }
            }
        };

        $this->on(ActiveRecord::EVENT_AFTER_INSERT, $save);
        $this->on(ActiveRecord::EVENT_AFTER_UPDATE, $save);

    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarts()
    {
        $db = \quoma\products\components\DbHelper::getDbName(Yii::$app->get('db'));
        return $this->hasMany(\frontend\models\Cart::className(), ['cart_id' => 'cart_id'])->viaTable("$db.cart_has_product", ['product_id' => 'product_id']);
    }
    
    /**
     * Agrega categorias al producto. Al hacer esto, se genera un evento para
     * que la relacion sea guardada luego de guardar el objeto.
     * TODO: link parents
     * @param type $categories
     */
    public function setCategories($categories){
        if(empty($categories)){
            $categories = [];
        }
        
        $this->_categories = $categories;

        $saveCategories = function($event){
            //Quitamos las relaciones actuales
            $this->unlinkAll('categories', true);
            //Guardamos las nuevas relaciones
            foreach ($this->_categories as $id){
                $this->link('categories', ProductCategory::findOne($id));
            }
        };

        $this->on(self::EVENT_AFTER_INSERT, $saveCategories);
        $this->on(self::EVENT_AFTER_UPDATE, $saveCategories);
    }
    
    /**
     * 
     * @param float $net_price
     * @param float $taxes
     * @param date $expiration yyyy/mm/dd
     */
    public function setPrice($net_price, $price_list, $before_price)
    {
        
        $price = new ProductPrice;
        $price->net_price = $net_price;
        $price->before_price = $before_price;
        $price->price_list_id = $price_list;
        $price->website_id = $this->website_id;
        
        //Relacionamos
        $this->link('productPrices',$price);
        
    }
    
    /**
     * 
     * @param float $net_price
     * @param float $taxes
     * @param date $expiration yyyy/mm/dd
     */
    public function setFinalPrice($final, $expiration)
    {
        
        $price = new ProductPrice;
        $price->net_price = $this->calculateNetPrice($final);
        
        /**
         * El precio almacena el resultado de aplicar todos los impuestos asociados.
         * Si es necesario el detalle, se debe utilizar el metodo getAllTaxes() 
         * de ProductPrice, o alternativamente el attr all
         */
        $price->taxes = $this->calculateTaxes($price->net_price);
        
        $price->exp_date = $expiration;
        $price->exp_timestamp = strtotime($expiration);
        
        //Relacionamos
        $this->link('productPrices',$price);
        
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductPrices()
    {
        return $this->hasMany(ProductPrice::className(), ['product_id' => 'product_id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActivePrice()
    {
        return $this->hasOne(ProductPrice::className(), ['product_id' => 'product_id'])
            ->where('exp_timestamp <= :now OR exp_timestamp = -1 OR exp_timestamp IS NULL',['now'=>time()])->orderBy(['product_price_id' => SORT_DESC])->cache(1);
    }
    
/**
     * @return \yii\db\ActiveQuery
     */
    public function getSecondaryUnit()
    {
        
        if(!WebsiteConfig::getWebsiteConfig('enable_secondary_stock', $this->website_id)->value){
            return null;
        }
        return $this->hasOne(Unit::className(), ['unit_id' => 'secondary_unit_id']);
    }
    

    /*** End Relations ***/
    
    /**
     * Devuelve el precio final actual del producto
     * TODO: Buscar el precio segun la lista
     * @return float
     */
    public function getFinalPrice()
    {
        
        $price = $this->getActivePrice()->one();
       
        if(!empty($price))
            return $price->net_price;
        
        return null;
        
    }

    /**
     * Devuelve el precio anterior actual del producto
     * TODO: Buscar el precio segun la lista
     * @return float
     */
    public function getBeforePrice()
    {

        $price = $this->getActivePrice()->one();

        if(!empty($price))
            return $price->before_price;

        return null;

    }


    /**
     * Devuelve el precio neto actual del producto
     * @return float
     */
    public function getNetPrice()
    {
        
        $price = $this->getActivePrice()->one();
        if(!empty($price))
            return $price->net_price;
        
        return null;
        
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPriceFromDate($date)
    {
        
        $query = $this->hasOne(ProductPrice::className(), ['product_id' => 'product_id'])
            ->where('timestamp <= :date_timestamp',['date_timestamp'=>strtotime($date)])
            ->orderBy(['timestamp' => SORT_DESC]);
        
        if($query->exists()){
            return $query;
        }

        //Si no encuentra precio para esa fecha
        return $this->getActivePrice();
    }
    
    /**
     * Calcula el precio neto en funcion de un precio final dado
     * @param float $final
     * @return float
     */
    public function calculateNetPrice($final)
    {
        
        $rates = 1+$this->calculateTaxRates();
        if($rates > 0){
            return $final / $rates;
        }else{
            return $final;
        }
        
    }
    
    /**
     * 
     * @param type $datas
     */
    public function savePrices($datas){
        
        if($datas){
            foreach ($datas as $data) {
                if($data['net_price'] != ''){
                    $price = new ProductPrice();
                    $price->load($data,'' );
                    $this->setPrice($price->net_price, $price->price_list_id, $price->before_price);
                }
            }
        }
    }

    public function hasCategory($category)
    {
        if(!is_object($category)){
            return $this->getCategories()->where(['product_category_id' =>
                ProductCategory::find()->select('product_category_id')->where(['system' => $category])])->cache()->exists();
        }

        return $this->getCategories()->where(['product_category_id' => $category->product_category_id])->cache()->exists();

    }
    
    
    public function getDeletable(){
        
         if($this->getCarts()->exists()){
            return false;
        }
        
        if($this->getStockMovements()->exists()){
            return false;
        }
        
        return true;
        
    }
    
     /**
     * Remueve los items sin stock del carrito
     * @return boolean
     */
    public function hasStock($qty){
            
        $stock = $this->getAvaibleStock(); 
        if($stock < $qty){
            return false;
        }
        return true;
    }
}
