<?php

namespace quoma\products\controllers;

use Yii;
use quoma\products\models\Product;
use quoma\products\models\search\ProductSearch;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ProductController implements the CRUD actions for Product model.
 */
class ProductController extends \quoma\products\components\Controller {
     
    protected function setWebsite($website)
    {
        $this->website = $website;
    }

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
   
    /**
     * Lists all Product models.
     * @return mixed
     */
    public function actionIndex() {

        $searchModel = new ProductSearch();
        
        $searchModel->website_id = $this->website->website_id;

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Product model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
         $model = $this->findModel($id);

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new Product model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {

        $model = new Product();
        $price = new \quoma\products\models\ProductPrice();
        
        $model->website_id = $this->website->website_id;
        $price->website_id = $this->website->website_id;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if(isset(Yii::$app->request->post()['ProductPrice'])){
                $model->savePrices(Yii::$app->request->post()['ProductPrice']);
            }

            return $this->redirect(['view', 'id' => $model->product_id]);
        } else {
            $products = ArrayHelper::map(Product::find()->andWhere(['status' => 'enabled', 'website_id' => $this->website->website_id])->all(), 'product_id', 'name');

            return $this->render('create', [
                'model' => $model,
                'price' => $price,
                'products' => $products
            ]);
        }
    }

    /**
     * Updates an existing Product model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        $price = new \quoma\products\models\ProductPrice();
     
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->unlinkAll('productPrices',true);
            $model->savePrices(Yii::$app->request->post()['ProductPrice']);
            return $this->redirect(['view', 'id' => $model->product_id]);
        } else {
            $products = ArrayHelper::map(Product::find()->andWhere(['status' => 'enabled', 'website_id' => $this->website->website_id])->all(), 'product_id', 'name');

            return $this->render('update', [
                'model' => $model,
                'price' => $price,
                'products' => $products
            ]);
        }
    }

    /**
     * Deletes an existing Product model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $model = $this->findModel($id);
        
        $model->unlinkAll('productPrices',true);
        $model->unlinkAll('categories',true);
        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Product model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Product the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Product::findOne($id)) !== null) {

            if($model->website_id != $this->website->website_id){
                throw new NotFoundHttpException('The requested page does not exist.');
            }

            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
