<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace quoma\products\controllers;

use Yii;
use quoma\products\models\StockMovement;
use quoma\products\models\search\StockMovementSearch;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use quoma\products\models\Product;

/**
 * Description of StockMovement
 *
 * @author Gabriela
 */
class StockMovementController extends \quoma\products\components\Controller 
{
      protected function setWebsite($website)
    {
        $this->website = $website;
    }

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }


    /**
     * Creates a new StockMovement model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($product_id, $type = 'in')
    {
        $model = Yii::$app->getModule('product')->stock->createMove();

        $product = Product::findOne($product_id);
        $model->website_id = $this->website->website_id;
        if($product === null)
            throw new NotFoundHttpException('The requested page does not exist.');

        if($type !== null)
            $model->type = $type;
        
        $model->product_id = $product->product_id;
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->stock_movement_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
    
     /**
     * Finds the StockMovement model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return StockMovement the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = StockMovement::findOne($id)) !== null) {

            if($model->website_id != $this->website->website_id){
                throw new NotFoundHttpException('The requested page does not exist.');
            }
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
}
