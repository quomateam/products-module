<?php

namespace quoma\products\controllers;

use Yii;
use quoma\products\models\Unit;
use quoma\products\models\search\UnitSearch;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UnitController implements the CRUD actions for Unit model.
 */
class UnitController extends \quoma\products\components\Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    protected function setWebsite($website) {
        $this->website = $website;
    }

    /**
     * Lists all Unit models.
     * @return mixed
     */
    public function actionIndex($website_id)
    {
        $searchModel = new UnitSearch();
        if($website_id){
            $website = \common\models\search\Website::findOne($website_id);
            $searchModel->website_id = $website_id;
            $this->setWebsite($website);
        }
        
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Unit model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        
        if ($model->website) {
            $this->setWebsite($model->website);
        }
        
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new Unit model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($website_id)
    {
        $model = new Unit();
        
         if($website_id){
            $website = \common\models\search\Website::findOne($website_id);
            $model->website_id = $website->website_id;
            $this->setWebsite($website);
        }
        

        if ($model->load(Yii::$app->request->post())) {
            $model->website_id = $website_id;
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->unit_id]);
            } else {
                \quoma\core\helpers\FlashHelper::flashErrors($model);
            }
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Unit model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        
        if ($model->website) {
            $this->setWebsite($model->website);
        }

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->unit_id]);
            } else {
                \quoma\core\helpers\FlashHelper::flashErrors($model);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Unit model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
         $model = $this->findModel($id);
        
        $website_id = $model->website_id;
        $model->delete();

        return $this->redirect(['index', 'website_id' => $website_id]);
    }

    /**
     * Finds the Unit model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Unit the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Unit::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
