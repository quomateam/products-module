<?php
namespace quoma\products\controllers;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
use frontend\models\Cart;
use \quoma\checkout\models\WebPayment;
use quoma\checkout\modules\siteConfig\models\SiteConfig;
use quoma\core\helpers\DbHelper;
use yii\console\Controller;
use \frontend\models\Website;
use common\modules\websiteFeature\features\EcommerceFeature;
use frontend\modules\websiteConfig\models\WebsiteConfig;
use quoma\products\models\StockMovement;

/**
 * Description of WebPaymentCommand
 *
 * @author Usuario
 */
class WebPaymentController extends Controller
{
    /**
     * Chequea si se ha confirmado el pago sino desactiva la reserva de stock
     */
    public function actionCheckPaymentStockReservation() {

        echo "Corriendo";
        echo "\n";

        //Busco los websites que tengan la feature ecommerce
        $websites = Website::find()
                ->innerJoin('website_has_checkout_site','website_has_checkout_site.website_id=website.website_id' )
                ->innerJoin( 'website_has_feature', 'website_has_feature.website_id=website.website_id')
                ->innerJoin( 'website_feature', 'website_feature.website_feature_id=website_has_feature.website_feature_id')
                ->where(['website_feature.slug' => 'ecommerce'])
                ->all();

        if(!$websites){
            echo "No se encontraron Websites.. FIN!";
            return ;
        }

        foreach ($websites as $website) {

            echo "WebSite $website->name";
            echo "\n";
        
            $currentDate = time();
            //minutos de espera por una respuesta de MP
            $timeout = $website->checkoutSite->platform_timeout;
             //Reservas con mas de x minutos deben desactivarse
            $firstTime = $currentDate - (60 * $timeout);

            //Solo revisamos reservas de las ultimas 72hs:
            $fromTime = $currentDate - (72 * 60 * 60);
            
            echo "CurrentTime=" . date('H:i:s', $currentDate);
            echo "\n";
            echo "FirstTime=" . date('H:i:s', $firstTime);
            echo "\n";


//            select *
//            from base_web_checkout.web_payment as wp
//            INNER JOIN cart ON cart.uuid = wp.request_uuid
//            INNER JOIN base_web_product.stock_movement AS sm ON sm.cart_id = cart.cart_id
//            WHERE sm.create_at >= '1524232800' AND sm. create_at <= '1525699191'
//            AND sm.type = 'r_out' AND sm.active = 1

            $bw_db = DbHelper::getDbName();
            $product_db = DbHelper::getDbName('db_product');

            //busco los pagos con reservas activas de stock
            $webPayments = WebPayment::find()
            ->innerJoin("$bw_db.cart", 'cart.uuid = web_payment.request_uuid')
            ->innerJoin("$product_db.stock_movement", ' stock_movement.cart_id = cart.cart_id')
            ->where("stock_movement.create_at >= $fromTime AND stock_movement.create_at <= $firstTime")
            ->andWhere(['stock_movement.type' => 'r_out', 'stock_movement.active' => 1])
            ->all();

            echo "Pagos encontrados: " . count($webPayments);
            echo "\n";
            
            //Por cada reserva no marcada como pagada, verificamos 
            foreach ($webPayments as $web_payment) {

                $cart = Cart::find()->where(['uuid' => $web_payment->request_uuid])->one();

                //cancelo el pago
                $result = $cart->cancelPayment($web_payment);
                if($result){
                    //cambio el carrito segun el resultado de la cancelacion
                    $cart->cancel($result['status']);
                }
            }
        }
        echo "Fin";
        echo "\n";
    }
    
}
